import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import RepeatOfNumbers, { NumberItem } from '../app/components/mainbody/RepeatOfNumbers';
import { LanguageProvider } from '@/app/components/mainbody/context/LanguagesContext';
import { convertNumberByLanguage } from "@/app/components/mainbody/convert-number/ConverNumber";

const meta: Meta<typeof RepeatOfNumbers> = {
    title: 'Components/RepeatOfNumbers',
    component: RepeatOfNumbers,
    decorators: [
        (Story) => (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '100vh',
                backgroundColor: '#f3f4f6',
            }}>
                <Story />
            </div>
        )
    ],
    argTypes: {
        number: {
            control: 'select',
            options: [11, 22, 44, 88, 176, 352, 704, 1408, 2816],
            defaultValue: 352,
        },
        repeat: {
            control: 'select',
            options: [0, 1, 2, 3],
            defaultValue: 0,
        }
    },
};

export default meta;

export const ActiveTrophy: StoryObj<typeof RepeatOfNumbers> = {
    // @ts-ignore
    render: ({ number, repeat }) => {
        const sampleNumbers: NumberItem[] = [
            {
                number: number,
                repeat: repeat,
                isActive: true,
            }
        ];

        return <div className='text-center' style={{ position: 'relative', display: 'inline-block' }}>
            <img
                src={sampleNumbers[0].isActive ? "/images/trophy.png" : "/images/trophy (1).png"}
                alt={`Number ${sampleNumbers[0].number}`}
                style={{
                    height: '128px',
                    width: '128px',
                    objectFit: 'cover',
                    filter: sampleNumbers[0].isActive ? 'none' : 'grayscale(100%) opacity(40%)'
                }}
            />
            <span style={{
                position: 'absolute',
                top: '25%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                fontSize: '1.875rem',
                color: '#4a5568',
                fontWeight: 'bold',
                zIndex: 1,
                fontFamily: 'WinterStorm'
            }}>
                {convertNumberByLanguage(sampleNumbers[0].repeat.toString(), 'en')}
            </span>
            <label style={{
                display: 'block',
                marginTop: '-10px',
                fontSize: '3rem',
                color: '#4a5568',
                fontWeight: 'bold',
                textAlign: 'center',
                fontFamily: 'WinterStorm'
            }}>
                {sampleNumbers[0].number}
            </label>
        </div>;
    },
    args: {
        number: 352,
        repeat: 0
    }
};

export const InactiveTrophy: StoryObj<typeof RepeatOfNumbers> = {
    // @ts-ignore
    render: ({number, repeat}) => {
        const sampleNumbers: NumberItem[] = [
            {
                number: number,
                repeat: repeat,
                isActive: false,
            }
        ];

        return <div className='text-center' style={{ position: 'relative', display: 'inline-block' }}>
            <img
                src="/images/trophy (1).png"
                alt={`Number ${sampleNumbers[0].number}`}
                style={{
                    height: '128px',
                    width: '128px',
                    objectFit: 'cover',
                    filter: sampleNumbers[0].isActive ? 'none' : 'grayscale(100%) opacity(40%)'
                }}
            />
            <span style={{
                position: 'absolute',
                top: '25%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                fontSize: '1.875rem',
                color: '#4a5568',
                fontWeight: 'bold',
                zIndex: 1,
                fontFamily: 'WinterStorm'
            }}>
                {convertNumberByLanguage(sampleNumbers[0].repeat.toString(), 'en')}
            </span>
            <label style={{
                display: 'block',
                marginTop: '-10px',
                fontSize: '3rem',
                color: '#4a5568',
                fontWeight: 'bold',
                textAlign: 'center',
                fontFamily: 'WinterStorm'
            }}>
                {convertNumberByLanguage(sampleNumbers[0].number.toString(), 'en')}
            </label>
        </div>;
    },
    args: {
        number: 352, // Default value for demonstration
        repeat: 0   // Default repeat count
    }
};