import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Buttons from '../app/components/mainbody/Buttons';
import './fonts.css';

interface ButtonProps {
    width: string;
    height: string;
    backgroundColor: string;
    borderRadius: string;
    fontSize: string;
    color: string;
    borderWidth: string;
    borderColor: string;
    borderStyle: string;
    boxShadow: string;
    hoverBackgroundColor: string;
    hoverColor: string;
}

const meta: Meta<ButtonProps> = {
    title: 'Components/Buttons',
    component: Buttons,
    decorators: [
        (Story) => (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
            }}>
                <Story />
            </div>
        )
    ],
    argTypes: {
        width: { control: 'text', defaultValue: '14rem' },
        height: { control: 'text', defaultValue: '5rem' },
        backgroundColor: { control: 'color', defaultValue: '#cbd5e0' },
        borderRadius: { control: 'text', defaultValue: '1.5rem' },
        fontSize: { control: 'text', defaultValue: '1.875rem' },
        color: { control: 'color', defaultValue: '#ffffff' },
        borderWidth: { control: 'text', defaultValue: '2px' },
        borderColor: { control: 'color', defaultValue: '#4b5563' },
        borderStyle: { control: 'select', options: ['none', 'solid', 'dotted', 'dashed', 'double', 'groove', 'ridge', 'inset', 'outset'], defaultValue: 'solid' },
        boxShadow: { control: 'text', defaultValue: '0px 4px 6px rgba(0, 0, 0, 0.1)' },
        hoverBackgroundColor: { control: 'color', defaultValue: '#4b5563' },
        hoverColor: { control: 'color', defaultValue: '#ffffff' }
    }
};

export default meta;

export const NewGameButton: StoryObj<ButtonProps> = {
    args: {
        width: '14rem',
        height: '5rem',
        backgroundColor: '#cbd5e0',
        borderRadius: '1.5rem',
        fontSize: '2rem',
        color: 'white',
        borderWidth: '2px',
        borderColor: '#4b5563',
        borderStyle: 'solid',
        boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.1)',
        hoverBackgroundColor: '#4b5563',
        hoverColor: '#ffffff'
    },
    render: (args) => (
        <button style={{
            width: args.width,
            height: args.height,
            backgroundColor: args.backgroundColor,
            borderRadius: args.borderRadius,
            fontSize: args.fontSize,
            color: args.color,
            borderWidth: args.borderWidth,
            borderColor: args.borderColor,
            borderStyle: args.borderStyle,
            boxShadow: args.boxShadow,
            transition: 'all 0.3s ease',
            cursor: 'pointer',
            fontFamily: 'WinterStorm, sans-serif'
        }}
                onMouseEnter={(e) => {
                    e.currentTarget.style.backgroundColor = args.hoverBackgroundColor;
                    e.currentTarget.style.color = args.hoverColor;
                }}
                onMouseLeave={(e) => {
                    e.currentTarget.style.backgroundColor = args.backgroundColor;
                    e.currentTarget.style.color = args.color; // Fixed to reflect current color
                }}
        >
            New Game
        </button>
    )
};
