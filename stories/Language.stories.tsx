import React from 'react';
import { Meta } from '@storybook/react';
import Languages from '../app/components/mainbody/Languages';
import { LanguageProvider } from '@/app/components/mainbody/context/LanguagesContext';
import { useTranslation } from "@/public/translation/translation";

export default {
    title: 'Components/Languages',
    component: Languages,
    decorators: [
        (StoryComponent) => (
            <LanguageProvider>
                <div style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
                    <StoryComponent />
                </div>
            </LanguageProvider>
        )
    ],
} as Meta<typeof Languages>;

const Template = () => {
    const { t } = useTranslation();
    return (
        <div>
            <Languages />
            <p style={{ fontFamily: 'WinterStorm, sans-serif' }}>{t('slm')}</p>
        </div>
    );
};

export const TranslateCheck = Template.bind({});
