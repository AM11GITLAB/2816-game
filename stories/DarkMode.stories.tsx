import React from 'react';
import { Meta, StoryFn } from '@storybook/react';
import DarkMode from '../app/components/header/DarkMode';

export default {
    title: 'Components/DarkMode',
    component: DarkMode,
    decorators: [
        (StoryComponent: StoryFn) => (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '100vh',
                backgroundColor: '#f3f4f6',
            }}>
                <StoryComponent />
            </div>
        )
    ],
} as Meta<typeof DarkMode>;

// @ts-ignore
const Template: StoryFn<typeof DarkMode> = (args: React.JSX.IntrinsicAttributes) => <DarkMode {...args} />;

export const Default = Template.bind({});
Default.args = {};
