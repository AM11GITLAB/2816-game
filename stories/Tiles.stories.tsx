import React from 'react';
import { Meta, StoryObj } from '@storybook/react';
import Tile from '../app/components/mainbody/Grid/Tile';
import { Tile16Props } from '@/app/components/mainbody/Grid/Tile';

interface SizeConfig {
    width: string;
    height: string;
    fontSize: string;
}

const sizeMapping: Record<string, SizeConfig> = {
    small: { width: '56px', height: '56px', fontSize: '16px' },
    medium: { width: '112px', height: '112px', fontSize: '36px' },
    large: { width: '168px', height: '168px', fontSize: '48px' },
};

const getSizeConfig = (sizeKey: string): SizeConfig => sizeMapping[sizeKey] || sizeMapping.medium;


const meta: Meta<typeof Tile> = {
    title: 'Components/Tiles',
    component: Tile,
    decorators: [
        (Story) => (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '100vh',
                width: '100%',
                backgroundColor: '#f3f4f6',
            }}>
                <Story />
            </div>
        )
    ],
    argTypes: {
        // @ts-ignore
        backgroundColor: { control: 'color' },
        textColor: { control: 'color' },
        number: {
            control: 'select',
            options: [11, 22, 44, 88, 176, 352, 704, 1408, 2816],
        },
        size: {
            control: 'select',
            options: ['small', 'medium', 'large'],
        },
        fontFamily: {
            control: 'select',
            options: ['WinterStorm', 'LunchFox', 'Jersey15-Regular', 'Vertically', 'DEMITHS', 'CuteDino', 'Chelsey'],
        },
        fontSize: {
            control: 'text',
        },
    },
};

export default meta;


export const TileExample: StoryObj<Tile16Props> = {
    args: {
        // @ts-ignore
        backgroundColor: '#4f46e5',
        textColor: '#ffffff',
        number: 11,
        size: 'medium',
        fontFamily: 'WinterStorm',
        fontSize: '36px',
    },
    // @ts-ignore
    render: ({ backgroundColor, textColor, number, size, fontFamily, fontSize }) => {
        const sizeConfig = getSizeConfig(size);
        return (
            <div style={{
                backgroundColor,
                color: textColor,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: '8px',
                width: sizeConfig.width,
                height: sizeConfig.height,
                fontFamily,
                fontSize: sizeConfig.fontSize,
            }}>
                {number}
            </div>
        );
    }
};
