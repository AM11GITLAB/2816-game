import React from 'react';
import { Meta, StoryObj, StoryContext } from '@storybook/react';
import ScoreBoard  from '../app/components/mainbody/scoreboard/ScoreBoard';
import { ScoreContext, ScoreContextType } from '@/app/components/mainbody/context/ScoresContext';
import './fonts.css';

interface ScoreBoardProps {
    bestScore: number;
    width: string;
    height: string;
    backgroundColor: string;
    borderRadius: string;
    fontSize: string;
    color: string;
    borderWidth: string;
    borderColor: string;
    borderStyle: string;
    boxShadow: string;
}

// Use the ScoreBoardProps interface to type the args

const meta: Meta<typeof ScoreBoard> = {
    title: 'Components/ScoreBoard',
    component: ScoreBoard,
    decorators: [
        // @ts-ignore
        (Story, context: StoryContext<ScoreBoardProps>) => {  // <-- Here is the update
            // @ts-ignore
            const scoreMock: ScoreContextType = {
                currentScore: 0,
                bestScore: context.args.bestScore,
                limitedCurrentScore: 0,
                mergedScores: 0,
                showAddedScore: false,
                is2816: false,
                setCurrentScoresArr: () => {},
                setLimitedCurrentScoresArr: () => {},
                setIs2816: (value: boolean) => {}
            };

            return (
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh',
                }}>
                    <ScoreContext.Provider value={scoreMock}>
                        <div className="scoreboard-wrapper" style={{
                            display: 'flex',
                            flexDirection: 'column',
                            width: context.args.width, // Now properly typed
                            height: context.args.height,
                            backgroundColor: context.args.backgroundColor,
                            borderRadius: context.args.borderRadius,
                            fontSize: context.args.fontSize,
                            justifyContent: 'center',
                            alignItems: 'center',
                            color: context.args.color,
                            borderWidth: context.args.borderWidth,
                            borderColor: context.args.borderColor,
                            borderStyle: context.args.borderStyle,
                            boxShadow: context.args.boxShadow,
                            boxSizing: 'border-box'
                        }}>
                            <h2 className='m-auto text-gray-700 text-3xl' style={{marginBottom: '0.2rem', fontFamily: 'WinterStorm, sans-serif' }} >high Score</h2>
                            <h3 className='m-auto text-gray-100 text-3xl' style={{marginTop: '0.2rem', fontFamily: 'WinterStorm, sans-serif' }} >{scoreMock.bestScore}</h3>
                        </div>
                    </ScoreContext.Provider>
                </div>
            );
        }
    ],
    argTypes: {
        bestScore: { control: 'number', defaultValue: 200 },
        width: { control: 'text', defaultValue: '14rem' },
        height: { control: 'text', defaultValue: '5rem' },
        backgroundColor: { control: 'color', defaultValue: '#cbd5e0' },
        borderRadius: { control: 'text', defaultValue: '1.5rem' },
        fontSize: { control: 'text', defaultValue: '1rem' },
        color: { control: 'color', defaultValue: '#ffffff' },
        borderWidth: { control: 'text', defaultValue: '2px' },
        borderColor: { control: 'color', defaultValue: '#4b5563' },
        borderStyle: { control: 'select', options: ['none', 'solid', 'dotted', 'dashed', 'double', 'groove', 'ridge', 'inset', 'outset'], defaultValue: 'solid' },
        boxShadow: { control: 'text', defaultValue: '0px 4px 6px rgba(0, 0, 0, 0.1)' }
    }
};

export default meta;

export const BestScoreOnly: StoryObj<typeof ScoreBoard> = {
    args: {
        bestScore: 200,
        width: '14rem',
        height: '5rem',
        backgroundColor: '#cbd5e0',
        borderRadius: '1.5rem',
        fontSize: '1rem',
        color: 'white',
        borderWidth: '2px',
        borderColor: '#4b5563',
        borderStyle: 'solid',
        boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.1)'
    }
};
