import { useContext } from 'react';
import { LanguageContext } from '../../app/components/mainbody/context/LanguagesContext';

interface Translations {
    [key: string]: {
        [key: string]: string;
    };
}

const translations: Translations = {
    iran: {
        undo: "بازگشت", newGame: "بازی جدید", help: "راهنما",
        currentScore: 'امتیاز فعلی', highScore: 'بالاترین امتیاز',
        scoresWall: 'دیوار امتیازات',
        lightMode: 'حالت روشن', darkMode: 'حالت تاریک',
        restart: 'راه اندازی مجدد', gameOver: '!باختی',
        ogp1: 'فوق‌العاده! شما عملکرد عالی داشتید و رکورد جدیدی ثبت کردید. حالا با تلاش بیشتر می‌توانید این رکورد را حتی بالاتر ببرید!',
        ogp2: 'امتیاز فعلی شما پایین است، اما با کمی تلاش بیشتر مطمئناً می‌توانید پیشرفت کنید!',
        ogp3: 'عملکرد شما خوب بود و با کمی تلاش بیشتر می‌توانید به نتایج عالی دست پیدا کنید!',
        ogp4: 'عملکرد شما عالی بود و امتیاز خوبی کسب کردید. آفرین! با کمی تلاش می‌توانید به بالاترین امتیاز برسید. ادامه دهید!',
        slm: 'سلام'
    },
    usa: {
        undo: "Undo", newGame: "New Game", help: "Help",
        currentScore: 'current score', highScore: 'high score',
        scoresWall: 'Scores Wall',
        lightMode: 'Light Mode', darkMode: 'Dark Mode',
        restart: 'Restart', gameOver: 'Game Over!',
        ogp1: 'Incredible! You performed excellently and set a new record. Now, with more effort, you can push this record even higher!',
        ogp2: 'Your current score is low, but with a little more effort, you can surely improve!',
        ogp3: 'Your performance was good, and with a little more effort, you can achieve excellent results!',
        ogp4: 'Your performance was great, and you scored well. Well done! With a bit more effort, you can reach the highest score. Keep it up!',
        slm: 'Hello'
    },
    china: {
        undo: "撤消", newGame: "新游戏", help: "帮助",
        currentScore: '当前分数', highScore: '高分',
        scoresWall: '分数墙',
        lightMode: '光照模式', darkMode: '深色模式',
        restart: '重新开始', gameOver: '游戏结束!',
        ogp1: '太棒了！你的表现非常出色，并创造了新的记录。现在，通过更多的努力，你可以进一步打破这个记录！',
        ogp2: '你目前的分数较低，但只要再多努力一点，你一定可以进步！',
        ogp3: '你的表现不错，只要再多努力一点，你可以取得优秀的成绩！',
        ogp4: '你的表现很棒，得到了很好的分数。干得好！只要再多努力一点，你可以达到最高分。继续加油！',
        slm: '你好'
    },
    france: {
        undo: "annuler", newGame: "nouveau jeu", help: "aide",
        currentScore: 'score actuel', highScore: 'score élevé',
        scoresWall: 'mur de partitions',
        lightMode: 'Mode lumière', darkMode: 'Mode sombre',
        restart: 'redémarrage', gameOver: 'jeu terminé!',
        ogp1: 'Incroyable! Vous avez excellé et établi un nouveau record. Maintenant, avec plus d\'effort, vous pouvez encore améliorer ce record!',
        ogp2: 'Votre score actuel est bas, mais avec un peu plus d\'effort, vous pouvez sûrement vous améliorer!',
        ogp3: 'Votre performance était bonne et avec un peu plus d\'effort, vous pouvez obtenir des résultats excellents!',
        ogp4: 'Votre performance était excellente et vous avez obtenu un bon score. Bravo! Avec un peu plus d\'effort, vous pouvez atteindre le score le plus élevé. Continuez comme ça!',
        slm: 'Bonjour'
    },
    southKorea: {
        undo: "실행 취소", newGame: "새로운 게임", help: "돕다",
        currentScore: '현재 점수', highScore: '높은 점수',
        scoresWall: '점수벽',
        lightMode: '조명 모드', darkMode: '다크 모드',
        restart: '재시작', gameOver: '게임 끝!',
        ogp1: '대단해요! 당신은 훌륭한 성과를 내고 새로운 기록을 세웠어요. 이제 더 노력하면 이 기록을 더 높일 수 있어요!',
        ogp2: '현재 점수가 낮지만, 조금만 더 노력하면 분명히 향상될 수 있어요!',
        ogp3: '당신의 성과는 좋았고, 조금만 더 노력하면 훌륭한 결과를 얻을 수 있어요!',
        ogp4: '당신의 성과는 훌륭했고, 좋은 점수를 받았어요. 잘했어요! 조금만 더 노력하면 최고 점수에 도달할 수 있어요. 계속하세요!',
        slm: '안녕하세요'
    },
    spain: {
        undo: "deshacer", newGame: "nuevo juego", help: "ayuda",
        currentScore: 'puntuación actual', highScore: 'puntuación más alta',
        scoresWall: 'pared de puntuaciones',
        lightMode: 'Modo de luz', darkMode: 'Modo oscuro',
        restart: 'Reanudar', gameOver: 'juego terminado!',
        ogp1: '¡Increíble! Tuviste un desempeño excelente y estableciste un nuevo récord. Ahora, con más esfuerzo, ¡puedes superar este récord aún más!',
        ogp2: 'Tu puntuación actual es baja, pero con un poco más de esfuerzo, ¡seguro que puedes mejorar!',
        ogp3: 'Tu desempeño fue bueno y con un poco más de esfuerzo, ¡puedes lograr resultados excelentes!',
        ogp4: 'Tu desempeño fue excelente y obtuviste una buena puntuación. ¡Bien hecho! Con un poco más de esfuerzo, puedes alcanzar la puntuación más alta. ¡Sigue así!',
        slm: 'Hola'
    },
    saudiArabia: {
        undo: "الغاء التحميل", newGame: "لعبة جديدة", help: "يساعد",
        currentScore: 'النتيجة الحالية', highScore: 'درجة عالية',
        scoresWall: 'عشرات الجدار',
        lightMode: 'وضع الضوء', darkMode: 'الوضع المظلم',
        restart: 'إعادة تشغيل', gameOver: '!انتهت اللعبة',
        ogp1: 'رائع! لقد أدّيت أداءً ممتازاً وسجّلت رقماً قياسياً جديداً. الآن، مع المزيد من الجهد، يمكنك رفع هذا الرقم القياسي أكثر!',
        ogp2: 'درجتك الحالية منخفضة، ولكن مع المزيد من الجهد، يمكنك بالتأكيد تحسينها!',
        ogp3: 'كان أداؤك جيداً ومع قليل من الجهد الإضافي، يمكنك تحقيق نتائج ممتازة!',
        ogp4: 'كان أداؤك رائعاً وحصلت على درجة جيدة. أحسنت! مع قليل من الجهد، يمكنك الوصول إلى أعلى درجة. استمر!',
        slm: 'مرحبًا'
    },
};

export const useTranslation = () => {
    const { language } = useContext(LanguageContext);

    const t = (key: string): string => {
        return translations[language]?.[key] || key;
    };

    return { t };
};
