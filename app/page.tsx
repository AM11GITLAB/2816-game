// page.tsx
import dynamic from 'next/dynamic';
import { LanguageProvider } from "@/app/components/mainbody/context/LanguagesContext";
import { HelpProvider } from "@/app/components/mainbody/context/HelpContext";

const MainBody = dynamic(() => import('@/app/components/mainbody/MainBody'), { ssr: false });
const Footer = dynamic(() => import('@/app/components/footer/Footer'), { ssr: false });
const Header = dynamic(() => import('@/app/components/header/Header'), { ssr: false });
const ScoreProvider = dynamic(() => import('@/app/components/mainbody/context/ScoresContext').then(mod => mod.ScoreProvider), { ssr: false });
const TilesProvider = dynamic(() => import('@/app/components/mainbody/context/TileContext').then(mod => mod.TilesProvider), { ssr: false });

const HomePage = () => {
    return (
        <HelpProvider>
            <ScoreProvider>
                <TilesProvider>
                    <LanguageProvider>
                        <div className='flex flex-col h-screen justify-between'>
                            <Header />
                            <MainBody />
                            <Footer />
                        </div>
                    </LanguageProvider>
                </TilesProvider>
            </ScoreProvider>
        </HelpProvider>
    );
};

export default HomePage;
