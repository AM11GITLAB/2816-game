'use client'
import React, { useState } from 'react';
import { useTranslation } from "@/public/translation/translation";
import { useTiles } from "@/app/components/mainbody/context/TileContext";
import { useScore } from "@/app/components/mainbody/hooks/useScore";
import { useHelp } from "@/app/components/mainbody/context/HelpContext";
import {useLanguage} from "@/app/components/mainbody/context/LanguagesContext";

const Buttons = () => {
    const { t } = useTranslation();
    const { language } = useLanguage(); // Use this to get the current language
    const { resetGame, setIsNewGame, undo, findBestMove, isUndo, setIsUndo, history } = useTiles();
    const { resetCurrentScore } = useScore();
    const { showHelp, setHelpClicked } = useHelp();
    const [shakeId, setShakeId] = useState<string | null>(null);

    const triggerShake = (id: string) => {
        setShakeId(id);
        setTimeout(() => setShakeId(null), 1000);
    };

    const handleUndoClick = () => {
        undo();
        setIsUndo(true);
        if (history.length <= 1) {
            triggerShake('undo');
        }
    };

    const handleNewGameClick = () => {
        resetGame();
        resetCurrentScore();
        setIsNewGame(true);
    };

    const handleHelpClick = () => {
        setHelpClicked(true);
        const direction = findBestMove();
        if (direction) {
            showHelp(direction);
        } else {
            triggerShake('help');
        }
    };

    const textSizeClass = language === 'southKorea' ? 'text-2xl' : 'text-3xl';

    return (
        <div className='grid grid-flow-col gap-4'>
            <button
                onClick={handleUndoClick}
                className={`flex h-16 w-40 bg-gray-400 hover:bg-indigo-400 hover:text-white rounded-3xl ${textSizeClass} justify-center items-center text-gray-700 border-2 border-gray-500 dark:border-gray-500 shadow hover:shadow-lg shadow-gray-400 transition duration-600 ease-in-out hover:border-indigo-200 dark:hover:border-indigo-200 ${shakeId === 'undo' ? 'animate-shake' : ''}`}>
                {t('undo')}
            </button>
            <button
                onClick={handleNewGameClick}
                className={`flex h-16 w-40 bg-gray-400 hover:bg-indigo-400 hover:text-white rounded-3xl ${textSizeClass} justify-center items-center text-gray-700 border-2 border-gray-500 dark:border-gray-500 shadow hover:shadow-lg shadow-gray-400 transition duration-600 ease-in-out hover:border-indigo-200 dark:hover:border-indigo-200`}>
                {t('newGame')}
            </button>
            <button
                onClick={handleHelpClick}
                className={`flex h-16 w-40 bg-gray-400 hover:bg-indigo-400 hover:text-white rounded-3xl ${textSizeClass} justify-center items-center text-gray-700 border-2 border-gray-500 dark:border-gray-500 shadow hover:shadow-lg shadow-gray-400 transition duration-600 ease-in-out hover:border-indigo-200 dark:hover:border-indigo-200 ${shakeId === 'help' ? 'animate-shake' : ''}`}>
                {t('help')}
            </button>
        </div>
    );
};

export default Buttons;
