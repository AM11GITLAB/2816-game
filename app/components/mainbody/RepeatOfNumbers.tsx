'use client'
import React, { useEffect, useState } from 'react';
import { useTiles } from "@/app/components/mainbody/context/TileContext";
import { useTranslation } from "@/public/translation/translation";
import { convertNumberByLanguage } from "@/app/components/mainbody/convert-number/ConverNumber";
import { useLanguage } from "@/app/components/mainbody/context/LanguagesContext";

export interface NumberItem {
    number: number;
    repeat: number;
    isActive: boolean;
}

const RepeatOfNumbers = () => {
    const { t } = useTranslation();
    const { language } = useLanguage();
    const { tiles, mergeCounts, isNewGame, setIsNewGame, resetGame, isUndo, setIsUndo, mergeCountMap } = useTiles();
    const [numbers, setNumbers] = useState<NumberItem[]>([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const storedNumbers = localStorage.getItem('numbers');
        if (storedNumbers) {
            const parsedNumbers = JSON.parse(storedNumbers);
            if (parsedNumbers.length > 0) {
                localStorage.setItem('numbers', JSON.stringify(parsedNumbers));
                setNumbers(parsedNumbers);
            } else {
                setInitialNumbers();
            }
        } else {
            setInitialNumbers();
        }
        setIsLoading(false);
    }, []);

    const setInitialNumbers = () => {
        const defaultNumbers: NumberItem[] = [
            { number: 11, repeat: 0, isActive: false },
            { number: 22, repeat: 0, isActive: false },
            { number: 44, repeat: 0, isActive: false },
            { number: 88, repeat: 0, isActive: false },
            { number: 176, repeat: 0, isActive: false },
            { number: 352, repeat: 0, isActive: false },
            { number: 704, repeat: 0, isActive: false },
            { number: 1408, repeat: 0, isActive: false },
            { number: 2816, repeat: 0, isActive: false },
        ];
        setNumbers(defaultNumbers);
        localStorage.setItem('numbers', JSON.stringify(defaultNumbers));
    };

    useEffect(() => {
        if (!isLoading) {
            const maxTileValue = Math.max(...tiles.filter(tile => tile !== null).map(tile => tile?.value || 0), 0);

            let updatedNumbers = numbers.map(numberItem => {
                const isActive = numberItem.number <= maxTileValue;
                const newRepeatValue = mergeCountMap.get(numberItem.number) || 0;
                return { ...numberItem, repeat: newRepeatValue, isActive: isActive };
            });

            if (isUndo) {
                const lastActiveIndex = updatedNumbers.findIndex(numberItem => numberItem.number > maxTileValue);
                if (lastActiveIndex !== -1) {
                    for (let i = lastActiveIndex; i < updatedNumbers.length; i++) {
                        updatedNumbers[i].isActive = false;
                    }
                }
            }

            setNumbers(updatedNumbers);
            localStorage.setItem('numbers', JSON.stringify(updatedNumbers));
        }
        if (isNewGame) {
            setInitialNumbers();
            setIsNewGame(false);
        }

    }, [tiles, isNewGame, setIsNewGame, isUndo, setIsUndo, mergeCountMap]);

    if (isLoading) {
        return <div className='text-3xl'>Loading ...</div>;
    }

    const scoreWallTextSizeClass = language === 'southKorea' || 'china' ? 'text-2xl' : 'text-3xl';
    const textSizeClass = language === 'southKorea' || 'china' ? 'text-2xl' : 'text-3xl';
    const labelSizeClass = language === 'southKorea' || 'china' ? 'text-4xl' : 'text-5xl';

    return (
        <>
            <div className={`grid grid-cols-3 justify-items-center gap-16 h-min`}>
                <div className='col-span-3'>
                    <h2 className={`flex justify-center text-6xl border-b-8 border-gray-600 text-gray-800 dark:border-gray-500 dark:text-gray-200 ${scoreWallTextSizeClass}`}>
                        <b>{t('scoresWall')}</b></h2>
                </div>
                {numbers.map((item) => (
                    <div key={item.number} className='text-center h-min relative'>
                        <img
                            src={item.isActive ? "/images/trophy.png" : "/images/trophy (1).png"}
                            alt={`Number ${item.number}`}
                            className={`h-32 w-32 rounded-3xl object-cover ${item.isActive ? 'filter invert(100%)' : 'opacity-40 dark:filter dark:invert'}`}
                        />
                        <span className={`absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-[175%] ${textSizeClass} text-gray-700 dark:text-gray-300 ${item.isActive ? 'dark:text-gray-700' : ''}`}>{convertNumberByLanguage(item.repeat.toString(), language)}</span>
                        <label className={`mt-4 ${labelSizeClass} text-gray-700 dark:text-gray-300`}>{convertNumberByLanguage(item.number.toString(), language)}</label>
                    </div>
                ))}
            </div>
        </>
    );
}

export default RepeatOfNumbers;
