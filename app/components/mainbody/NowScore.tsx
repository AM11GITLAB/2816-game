'use client';
import React, { useState, useEffect } from 'react';
import { useTiles } from "@/app/components/mainbody/context/TileContext";
import { useLanguage } from "@/app/components/mainbody/context/LanguagesContext";
import { convertNumberByLanguage } from "@/app/components/mainbody/convert-number/ConverNumber";
let persistentTotalMergedScore = 0;

const NowScore = () => {
    const { tiles } = useTiles();
    const { language } = useLanguage();
    const [totalMergedScore, setTotalMergedScore] = useState(persistentTotalMergedScore);

    useEffect(() => {
        let total = tiles.reduce((acc, tile) => {
            return tile && tile.hasMerged ? acc + tile.value : acc;
        }, 0);
        console.log("t: " + total)
        console.log("p: " + persistentTotalMergedScore)
        persistentTotalMergedScore += total;
        total = 0;
        setTotalMergedScore(persistentTotalMergedScore);
    }, [tiles]);

    return (
        <aside className="overflow-auto min-h-96 bg-yellow-600 w-20">
            <div className="flex flex-col space-y-4 overflow-auto">
                {tiles.map((tile, index) => (
                    tile ? <label key={index}>{convertNumberByLanguage(tile.value.toString(), language)}</label> : null
                ))}
            </div>
            <div className="mt-4">
                <label>Total Merged Score: {convertNumberByLanguage(totalMergedScore.toString(), language)}</label>
            </div>
        </aside>
    );
};

export default NowScore;
