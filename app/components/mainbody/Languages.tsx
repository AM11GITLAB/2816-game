'use client'
import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useLanguage } from './context/LanguagesContext';

const Languages = () => {
    const [activeButton, setActiveButton] = useState(() => localStorage.getItem('language') || 'usa');
    const { switchLanguage } = useLanguage();

    const flags = [
        { id: 'iran', src: '/images/Flag-Iran.webp', alt: 'Iran' },
        { id: 'usa', src: '/images/Flag_of_the_United_States.png', alt: 'USA' },
        { id: 'china', src: '/images/china.png', alt: 'China' },
        { id: 'france', src: '/images/france.webp', alt: 'France' },
        { id: 'southKorea', src: '/images/korean-flag.jpeg', alt: 'South Korea' },
        { id: 'spain', src: '/images/España.svg', alt: 'Spain' },
        { id: 'saudiArabia', src: '/images/Saudi_Arabia.svg.png', alt: 'Saudi Arabia' },
    ];

    useEffect(() => {
        switchLanguage(activeButton);
        localStorage.setItem('language', activeButton);
    }, [activeButton, switchLanguage]);

    const handleButtonClick = (id: string) => {
        setActiveButton(id);
    };

    return (
        <aside className='grid gap-4'>
            {flags.map((flag) => (
                <button
                    key={flag.id}
                    onClick={() => handleButtonClick(flag.id)}
                    className={`border-2 ${activeButton === flag.id ? 'border-gray-500 border-3 shadow-xl' : 'border-gray-200 shadow-md hover:border-b-indigo-400'} rounded-md ${activeButton !== flag.id ? 'filter grayscale opacity-50' : ''} hover:border-gray-600`}
                >
                    <Image
                        src={flag.src}
                        alt={flag.alt}
                        width={60}
                        height={40}
                        objectFit="cover"
                    />
                </button>
            ))}
        </aside>
    );
};

export default Languages;
