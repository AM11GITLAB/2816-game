'use client'
import dynamic from 'next/dynamic';

const RepeatOfNumbers = dynamic(() => import("@/app/components/mainbody/RepeatOfNumbers"), { ssr: false });
const GameGrid = dynamic(() => import("@/app/components/mainbody/Grid/GameGrid"), { ssr: false });
const Languages = dynamic(() => import("@/app/components/mainbody/Languages"), { ssr: false });

const MainBody = () => {
    return (
        <div className='flex h-screen bg-white dark:bg-dark-200'>
            <main className='w-1/2 h-full justify-center items-center'>
                <div className='flex justify-center items-center h-full'>
                    <Languages />
                    <GameGrid />
                </div>
            </main>
            <section className='flex w-1/2 h-full justify-center items-center'>
                <RepeatOfNumbers />
            </section>
        </div>
    );
};

export default MainBody;
