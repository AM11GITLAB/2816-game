import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from "@/public/translation/translation";
import { convertNumberByLanguage } from "@/app/components/mainbody/convert-number/ConverNumber";
import { useLanguage } from "@/app/components/mainbody/context/LanguagesContext";
import { useScore } from "@/app/components/mainbody/hooks/useScore";

const ScoreBoard = () => {
    const { t } = useTranslation();
    const { language } = useLanguage();
    const {
        currentScore, bestScore, mergedScores, showAddedScore,
        limitedCurrentScore, setIs2816, is2816, setCurrentScoresArr, setLimitedCurrentScoresArr
    } = useScore();
    const [isGold, setIsGold] = useState(false);

    useEffect(() => {
        setCurrentScoresArr((currentScoresArr: number[]) => [...currentScoresArr, currentScore]);
    }, [currentScore, setCurrentScoresArr]);

    const isKoreanOrChinese = language === 'southKorea' || language === 'china' ? 'text-2xl' : 'text-3xl';


    return (
        <div className={`grid grid-flow-col min-w-40 justify-center gap-3`}>
            <div
                className={`relative flex flex-col w-56 h-20 bg-gray-400 rounded-3xl justify-center text-white border-2 border-gray-500 dark:border-gray-500 ${
                    isKoreanOrChinese
                }`}>
                <h2 className='m-auto text-gray-700'>{t('currentScore')}</h2>
                <h3 className='m-auto text-gray-100'>
                    {convertNumberByLanguage(currentScore.toString(), language)} -
                    {` ${convertNumberByLanguage(limitedCurrentScore.toString(), language)}`}
                </h3>
                {showAddedScore && mergedScores !== 0 && (
                    <div
                        className={`absolute text-gray-800 animate-float-up w-10 h-10 
                ${isGold ? 'text-yellow-500' : ''} 
                ${mergedScores <= 0 ? 'text-red-500' : ''}`}>
                        {mergedScores > 0 ? '+' : ''}
                        {convertNumberByLanguage(mergedScores.toString(), language)}
                    </div>
                )}
                {showAddedScore && limitedCurrentScore === 0 && currentScore !== 0 && (
                    <div className={`absolute animate-float-up w-10 h-10 ${isKoreanOrChinese} text-yellow-500 ml-20`}>
                        {convertNumberByLanguage('2816', language)}
                    </div>
                )}
            </div>
            <div
                className={`flex flex-col w-56 h-20 bg-gray-400 rounded-3xl justify-center text-white border-2 border-gray-500 dark:border-gray-500 ${
                    isKoreanOrChinese
                }`}>
                <h2 className='m-auto text-gray-700'>{t('highScore')}</h2>
                <h3 className='m-auto text-gray-100'>{convertNumberByLanguage(bestScore.toString(), language)}</h3>
            </div>
        </div>
    );
};

export default ScoreBoard;
