'use client'
import React, {createContext, useCallback, useContext, useEffect, useState} from 'react';
import {useTiles} from "@/app/components/mainbody/context/TileContext";
import {sum} from "lodash";

export interface ScoreContextType {
    currentScore: number;
    bestScore: number;
    simulateScoreAddition: (scoreToAdd: number) => void;
    resetCurrentScore: () => void;
    showAddedScore: boolean;
    mergedScores: number;
    resetMergedScores: () => void;
    setMergedScores: (score: number) => void;
    limitedCurrentScore: number;
    is2816: boolean;
    setIs2816: (is2816: boolean) => void;
    newCurrentScoresArray: number[];
    setCurrentScore: (score: number) => void;
    setLimitedCurrentScore: (score: number) => void;
    setCurrentScoresArr: (scores: (currentScoresArr: number[]) => number[]) => void;
    setLimitedCurrentScoresArr: (scores: number[]) => void;
}

export const ScoreContext = createContext<ScoreContextType | undefined>(undefined);

export const ScoreProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const [currentScore, setCurrentScore] = useState(() => {
        const savedCurrentScore = localStorage.getItem('currentScore');
        return savedCurrentScore ? parseInt(savedCurrentScore, 10) : 0;
    });
    const [limitedCurrentScore, setLimitedCurrentScore] = useState(() => {
        const savedLimitedCurrentScore = localStorage.getItem('limitedCurrentScore');
        return savedLimitedCurrentScore ? parseInt(savedLimitedCurrentScore, 10) : 0;
    });
    const [bestScore, setBestScore] = useState(() => {
        const savedBestScore = localStorage.getItem('bestScore');
        return savedBestScore ? parseInt(savedBestScore, 10) : 0;
    });
    const [mergedScores, setMergedScores] = useState(() => {
        const savedMergedScores = localStorage.getItem('mergedScores');
        return savedMergedScores ? parseInt(savedMergedScores, 10) : 0;
    });
    const [showAddedScore, setShowAddedScore] = useState(false);
    const [is2816, setIs2816] = useState(false);

    const [currentScoresArray, setCurrentScoresArray] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('currentScoresArray');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [newCurrentScoresArray, setNewCurrentScoresArray] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('newCurrentScoresArray');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [limitedCurrentScoresArray, setLimitedCurrentScoresArray] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('limitedCurrentScoresArray');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [newLimitedCurrentScoresArray, setNewLimitedCurrentScoresArray] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('newLimitedCurrentScoresArray');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [currentScoresArr, setCurrentScoresArr] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('currentScoresArr');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [limitedCurrentScoresArr, setLimitedCurrentScoresArr] = useState<number[]>(() => {
        const savedScores = localStorage.getItem('limitedCurrentScoresArr');
        return savedScores ? JSON.parse(savedScores) : [];
    });

    const [flag, setFlag] = useState(() => {
        const savedBestScore = localStorage.getItem('flag');
        return savedBestScore ? parseInt(savedBestScore, 10) : 0;
    });

    const [flag2816, setFlag2816] = useState<boolean>(() => {
        const savedFlag = localStorage.getItem('flag2816');
        return savedFlag ? JSON.parse(savedFlag) : false;
    });

    useEffect(() => {
        setCurrentScore(() => {
            const savedNewCurrentScoresArray = localStorage.getItem('newCurrentScoresArray');
            const parsedNewCurrentScoresArray = savedNewCurrentScoresArray ? JSON.parse(savedNewCurrentScoresArray) : [];

            let sumOfCs = Array.isArray(parsedNewCurrentScoresArray)
                ? parsedNewCurrentScoresArray.reduce((acc, score) => acc + score, 0) / 2
                : 0;

            if (sumOfCs > bestScore) {
                setBestScore(sumOfCs);
                localStorage.setItem('bestScore', sumOfCs.toString());
            }
            localStorage.setItem('currentScore', sumOfCs.toString());
            return sumOfCs;
        });
        setLimitedCurrentScore(prevLimitedCurrentScore => {
            const savedNewCurrentScoresArray = localStorage.getItem('newCurrentScoresArray');
            const parsedNewCurrentScoresArray = savedNewCurrentScoresArray ? JSON.parse(savedNewCurrentScoresArray) : [];
            let sumOfLcs = Array.isArray(parsedNewCurrentScoresArray)
                ? parsedNewCurrentScoresArray.reduce((acc, score) => acc + score, 0) / 2
                : 0;
            if (sumOfLcs !== 0) {
                setFlag2816(true);
            }
            sumOfLcs %= 2816;

           if (sumOfLcs === 0 && flag2816) {
                setIs2816(true);
                let add2816 = 2816;
               setCurrentScore(() => {
                   const savedNewCurrentScoresArray = localStorage.getItem('newCurrentScoresArray');
                   const parsedNewCurrentScoresArray = savedNewCurrentScoresArray ? JSON.parse(savedNewCurrentScoresArray) : [];
                   let sumOfCs = Array.isArray(parsedNewCurrentScoresArray)
                       ? parsedNewCurrentScoresArray.reduce((acc, score) => acc + score, 0) / 2
                       : 0;
                   sumOfCs += add2816;
                   if (sumOfCs > bestScore) {
                       setBestScore(sumOfCs);
                       localStorage.setItem('bestScore', sumOfCs.toString());
                   }
                   localStorage.setItem('currentScore', sumOfCs.toString());
                   return sumOfCs;
               });
            }



            localStorage.setItem('limitedCurrentScore', sumOfLcs.toString());
            return sumOfLcs;
        });
    }, [newCurrentScoresArray, bestScore]);



    const simulateScoreAddition = useCallback((scoreToAdd: number) => {

        setCurrentScoresArray((prevScoresArray: number[]) => {
            if (!Array.isArray(prevScoresArray)) {
                prevScoresArray = [];
            }
            const newScoresArray = [...prevScoresArray, scoreToAdd];
            localStorage.setItem('currentScoresArray', JSON.stringify(newScoresArray));

            if (newScoresArray.length !== 0 && newScoresArray.length % 4 === 0) {
                const sumOfLastFourScores = newScoresArray.slice(-4).reduce((acc, current) => acc + current, 0);
                let newArray: number[];
                try {
                    newArray = JSON.parse(localStorage.getItem('newCurrentScoresArray') || '[]');
                } catch (error) {
                    newArray = [];
                }
                if (!Array.isArray(newArray)) {
                    newArray = [];
                }
                newArray.push(sumOfLastFourScores);
                setNewCurrentScoresArray(newArray);
                localStorage.setItem('newCurrentScoresArray', JSON.stringify(newArray));
            }
            return newScoresArray;
        });

        setLimitedCurrentScoresArray((prevScoresArray: number[]) => {
                    if (!Array.isArray(prevScoresArray)) {
                        prevScoresArray = [];
                    }
                    const newScoresArray = [...prevScoresArray, scoreToAdd];
                    localStorage.setItem('limitedCurrentScoresArray', JSON.stringify(newScoresArray));

                    if (newScoresArray.length !== 0 && newScoresArray.length % 4 === 0) {
                        const sumOfLastFourScores = newScoresArray.slice(-4).reduce((acc, current) => acc + current, 0);
                        let newArray;
                        try {
                            newArray = JSON.parse(localStorage.getItem('newLimitedCurrentScoresArray') || '[]');
                        } catch (error) {
                            newArray = [];
                        }
                        if (!Array.isArray(newArray)) {
                            newArray = [];
                        }
                        newArray.push(sumOfLastFourScores);
                        localStorage.setItem('newLimitedCurrentScoresArray', JSON.stringify(newArray));
                    }
                    return newScoresArray;
                });

        setMergedScores(prevMergedScores => {
            let newMergedScores =  prevMergedScores + scoreToAdd;

            localStorage.setItem('mergedScores', newMergedScores.toString());
            return newMergedScores;
        });
        setShowAddedScore(true);
        setTimeout(() => setShowAddedScore(false), 2000);
    }, [bestScore]);

    const resetCurrentScore = useCallback(() => {
        setCurrentScore(0);
        setMergedScores(0);
        setLimitedCurrentScore(0);
        setCurrentScoresArray([]);
        setNewCurrentScoresArray([]);
        setLimitedCurrentScoresArray([]);
        setNewLimitedCurrentScoresArray([]);
        setFlag2816(false);
        localStorage.setItem('currentScore', '0');
        localStorage.setItem('limitedCurrentScore', '0');
        localStorage.setItem('mergedScores', '0');
        localStorage.setItem('currentScoresArray', '0');
        localStorage.setItem('newCurrentScoresArray', '0');
        localStorage.setItem('limitedCurrentScoresArray', '0');
        localStorage.setItem('newLimitedCurrentScoresArray', '0');
        localStorage.setItem('flag2816', JSON.stringify(flag2816));
    }, []);

    useEffect(() => {
        localStorage.setItem('bestScore', bestScore.toString());
    }, [bestScore]);

    const resetMergedScores = () => {
        setMergedScores(0);
        localStorage.setItem('mergedScores', '0');
    };

    return (
        <ScoreContext.Provider value={{ currentScore, bestScore, simulateScoreAddition, showAddedScore, resetCurrentScore,mergedScores, resetMergedScores,setMergedScores
            ,limitedCurrentScore,is2816, setIs2816,newCurrentScoresArray,setCurrentScore,setLimitedCurrentScore,setCurrentScoresArr,setLimitedCurrentScoresArr}}>
            {children}
        </ScoreContext.Provider>
    );
};

