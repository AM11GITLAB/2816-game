'use client'
import React, {createContext, useContext, useState, ReactNode, useEffect} from 'react';
import localFont from 'next/font/local';
import Languages from "@/app/components/mainbody/Languages";

const usaFont = localFont({
    src: "../../../../public/font/WinterStorm.ttf",
});

const iranFont = localFont({
    src: "../../../../public/font/Vazirmatn-Medium.ttf",
});
const saudiArabiaFont = localFont({
    src: "../../../../public/font/IBMPlexSansArabic-Regular.ttf",
});
const chinaFont = localFont({
    src: "../../../../public/font/SmileySans-Oblique.ttf",
});
const spainFont = localFont({
    src: "../../../../public/font/BebasNeue-Regular.ttf",
});
const franceFont = localFont({
    src: "../../../../public/font/Cafe Francoise_D.otf",
});
const koreaFont = localFont({
    src: "../../../../public/font/Maplestory OTF Light.otf",
});

interface LanguageContextType {
    language: string;
    switchLanguage: (language: string) => void;
}

export const LanguageContext = createContext<LanguageContextType>({
    language: 'usa',
    switchLanguage: () => {}
});

export const LanguageProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [language, setLanguage] = useState<string>('usa');

    const switchLanguage = (lang: string) => {
        setLanguage(lang);
    };

    useEffect(() => {
        console.log('Current font classes:', {
            usa: usaFont.className,
            iran: iranFont.className,
            saudiArabia: saudiArabiaFont.className,
            spain: spainFont.className,
            france: franceFont.className,
            china: chinaFont.className,
            korea: koreaFont.className
        });
        document.body.classList.remove(
            usaFont.className,
            iranFont.className,
            saudiArabiaFont.className,
            spainFont.className,
            franceFont.className,
            chinaFont.className,
            koreaFont.className
        );

        switch (language) {
            case 'usa':
                document.body.classList.add(usaFont.className);
                break;
            case 'iran':
                document.body.classList.add(iranFont.className);
                break;
            case 'saudiArabia':
                document.body.classList.add(saudiArabiaFont.className);
                break;
            case 'spain':
                document.body.classList.add(spainFont.className);
                break;
            case 'france':
                document.body.classList.add(franceFont.className);
                break;
            case 'china':
                document.body.classList.add(chinaFont.className);
                break;
            case 'southKorea':
                document.body.classList.add(koreaFont.className);
                break;
            default:
                break;
        }
    }, [language]);

    return (
        <LanguageContext.Provider value={{ language, switchLanguage }}>
            {children}
        </LanguageContext.Provider>
    );
};

export const useLanguage = (): LanguageContextType => useContext(LanguageContext);
