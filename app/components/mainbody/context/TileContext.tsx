'use client'
import React, {createContext, useCallback, useContext, useEffect, useState} from 'react';
import {createTile, ITile} from '../Grid/ITile';
import _ from "lodash";
import {useScore} from "@/app/components/mainbody/hooks/useScore";

export interface TilesContextType {
    tiles: (ITile | null)[];
    mergeCounts: Record<number, number>;
    moveTiles: (direction: 'left' | 'right' | 'up' | 'down', help: boolean) => void;
    resetGame: () => void;
    isNewGame: number;
    setIsNewGame: (value: boolean) => void;
    undo: () => void;
    findBestMove: () => string | null;
    isGameOver: boolean;
    setIsGameOver: (value: boolean) => void;
    isUndo: boolean;
    setIsUndo: (value: boolean) => void;
    setHasUsedHelp: (value: boolean) => void;
    hasUsedHelp: boolean;
    setIsClicked: (value: boolean) => void;
    mergeCountMap: Map<number, number>;
    history: (ITile | null)[][];
    resetMergedScores: () => void;
    resetCurrentScore: () => void;
}

interface IMergeHistory {
    [key: number]: number[];
}

type MergeCounterState = Record<number, number>;

const TilesContext = createContext<TilesContextType | undefined>(undefined);

export const createEmptyBoard = (): (ITile | null)[] => new Array(16).fill(null);

const getInitialMergeCountMap = () => {
    const storedMergeCountMap = localStorage.getItem('mergeCountMap');
    if (storedMergeCountMap) {
        const mergeCountArray = JSON.parse(storedMergeCountMap);
        return new Map(mergeCountArray);
    }
    return new Map();
};
export const TilesProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const [tiles, setTiles] = useState<(ITile | null)[]>(createEmptyBoard());
    const [mergeCounts, setMergeCounts] = useState<Record<number, number>>({});    const { currentScore, bestScore ,simulateScoreAddition, resetCurrentScore,is2816, setIs2816,setCurrentScore,setLimitedCurrentScore} = useScore();
    const [tilesHistory, setTilesHistory] = useState([createEmptyBoard()]);
    const [mergeHistory, setMergeTilesHistory] = useState([createEmptyBoard()]);
    const [isInitialized, setIsInitialized] = useState(false);
    const [isNewGame, setIsNewGame] = useState(0);
    const [isGameOver, setIsGameOver] = useState(false);
    const [isUndo, setIsUndo] = useState(false);
    const [hasUsedHelp, setHasUsedHelp] = useState(false);
    const [mergeCounter, setMergeCounter] = useState<MergeCounterState>({});
    const [isClicked, setIsClicked] = useState(false);
    const [mergedValues, setMergedValues] = useState<number[]>([]);
    const [pervMergedValues, setPervMergedValues] = useState<number[]>([]);
    const [mergeCountMap, setMergeCountMap] = useState<Map<number, number>>(getInitialMergeCountMap);

    const setTilesForTest = (newTiles: (ITile | null)[]) => {
        const createEmptyBoard = (): (ITile | null)[] => new Array(16).fill(null);
        setTiles(createEmptyBoard);
    };

    useEffect(() => {
        const storedMergeCountMap = localStorage.getItem('mergeCountMap');
        if (storedMergeCountMap) {
            const mergeCountArray = JSON.parse(storedMergeCountMap);
            setMergeCountMap(new Map(mergeCountArray));
        }
    }, []);

    useEffect(() => {
        const mergeCountArray = Array.from(mergeCountMap.entries());
        localStorage.setItem('mergeCountMap', JSON.stringify(mergeCountArray));
    }, [mergeCountMap]);
    function checkGameOver(tiles: (ITile | null)[]): boolean {
        for (let i = 0; i < 4; i++) {
            for (let j = 0; j < 4; j++) {
                const tile = tiles[i * 4 + j];
                if (tile === null) {
                    return false;
                }

                if (i > 0 && tiles[(i - 1) * 4 + j] && tiles[(i - 1) * 4 + j]?.value === tile.value) {
                    return false;
                }

                if (i < 3 && tiles[(i + 1) * 4 + j] && tiles[(i + 1) * 4 + j]?.value === tile.value) {
                    return false;
                }

                if (j > 0 && tiles[i * 4 + (j - 1)] && tiles[i * 4 + (j - 1)]?.value === tile.value) {
                    return false;
                }

                if (j < 3 && tiles[i * 4 + (j + 1)] && tiles[i * 4 + (j + 1)]?.value === tile.value) {
                    return false;
                }
            }
        }

        return true;
    }

    useEffect(() => {
        const gameOver = checkGameOver(tiles);
        if (gameOver) {
            setIsGameOver(true);
        }
    }, [tiles]);

    const addRandomTileToTiles = useCallback((currentTiles: (ITile | null)[]): (ITile | null)[] => {
        const updatedTiles = currentTiles.map(tile => tile ? { ...tile, isNew: false } : null);

        const emptyIndices = updatedTiles
            .map((tile, index) => tile === null ? index : null)
            .filter((index): index is number => index !== null);

        if (emptyIndices.length === 0) {
            return updatedTiles;
        }

        const randomIndex = emptyIndices[Math.floor(Math.random() * emptyIndices.length)];
        const newValue = Math.random() < 0.9 ? 11 : 22;
        const newTile = createTile(newValue, true);

        updatedTiles[randomIndex] = newTile;

        return updatedTiles;
    }, []);

    const resetGame = useCallback(() => {
        const initialTiles = addRandomTileToTiles(addRandomTileToTiles(createEmptyBoard()));
        setTiles(initialTiles);
        setMergeCounts({});
        resetCurrentScore();
        setTilesHistory([initialTiles]);
        setMergedValues([]);
        setMergeCountMap(new Map());
        setMergeTilesHistory([]);
        const mergeCountArray = Array.from(mergeCountMap.entries());
        localStorage.setItem('mergeCountMap', JSON.stringify(mergeCountArray));
        localStorage.setItem('tiles', JSON.stringify(initialTiles));
        localStorage.setItem('history', JSON.stringify([initialTiles]));
        localStorage.setItem('mergeHistory', JSON.stringify([]));
        localStorage.setItem('currentScores', JSON.stringify([]));
        localStorage.setItem('limitedCurrentScores', JSON.stringify([]));

    }, [addRandomTileToTiles]);

    const undo = useCallback(() => {
        setTilesHistory(prev => {
            if (prev.length > 1) {
                const newHistory = prev.slice(0, -1);
                const prevState = newHistory[newHistory.length - 1];

                setTiles(prevState);
                console.log(prevState);

                const mergedTilesValues = prevState
                    .flat()
                    .filter(tile => tile?.hasMerged)
                    .map(tile => tile?.value);

                const newMergeCountMap = new Map();
                mergedTilesValues.forEach(value => {
                    const count = newMergeCountMap.get(value) || 0;
                    newMergeCountMap.set(value, count + 1);
                });
                setMergeCountMap(newMergeCountMap);
                return newHistory;
            } else {
                return prev;
            }
        });

        try {
            let newArray = JSON.parse(localStorage.getItem('newCurrentScoresArray') || '[]');
            if (Array.isArray(newArray) && newArray.length > 0) {
                newArray.pop();
                newArray.pop();

                localStorage.setItem('newCurrentScoresArray', JSON.stringify(newArray));

                const sumCs = newArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0)/2;
                setCurrentScore(sumCs);
                localStorage.setItem('currentScore', sumCs.toString());

                let sumLcs = newArray.reduce((accumulator, currentValue) => accumulator + currentValue, 0)/2;
                sumLcs %= 2816;
                setLimitedCurrentScore(sumLcs);
                localStorage.setItem('limitedCurrentScore', sumLcs.toString());
            }
        } catch (error) {
            console.error("Error parsing newCurrentScoresArray from localStorage:", error);
        }

    }, []);




    useEffect(() => {
        const storedHistory = localStorage.getItem('history');
        if (storedHistory) {
            setTilesHistory(JSON.parse(storedHistory));
        } else {
            const initialTiles = addRandomTileToTiles(addRandomTileToTiles(createEmptyBoard()));
            setTilesHistory([initialTiles]);
            localStorage.setItem('history', JSON.stringify([initialTiles]));
        }
        setIsInitialized(true);
        localStorage.setItem('currentScores', JSON.stringify(currentScore));
        localStorage.setItem('limitedCurrentScores', JSON.stringify([]));
    }, []);


    useEffect(() => {
        localStorage.setItem('history', JSON.stringify(tilesHistory));
    }, [tilesHistory]);


    useEffect(() => {
        if (isInitialized) {
            if (tilesHistory.length === 0 || !_.isEqual(tilesHistory[tilesHistory.length - 1], tiles)) {
                setTilesHistory(prev => [...prev, tiles]);
            }
        }
    }, [tiles, isInitialized]);


    useEffect(() => {
        if (!isInitialized) {
            const tilesJson = localStorage.getItem('tiles');
            if (tilesJson) {
                const savedTiles = JSON.parse(tilesJson);
                setTiles(savedTiles);
            } else {
                resetGame();
            }
            setIsInitialized(true);
        }
    }, [resetGame, isInitialized]);

    useEffect(() => {
        if (isInitialized) {
            localStorage.setItem('tiles', JSON.stringify(tiles));
        }
    }, [tiles, isInitialized]);

    function updateMergeCountAfterMove() {

        setMergeCounts(mergeCounts);
        mergedValues.forEach(num => {
            const currentCount = mergeCountMap.get(num) || 0;
            mergeCountMap.set(num, currentCount + 1);
        });

        for (let [num, count] of mergeCountMap) {
            //console.log(`num = ${num}, count = ${count}`);
            if (!mergedValues.includes(num)) {
                mergeCountMap.set(num, 0);
            }

        }

        for (let [num, count] of mergeCountMap) {
            if (count >= 3) {
                //console.log(`عدد ${num} ${count} بار پشت سر هم مرج شده است.`);
                mergeCountMap.set(num, count - 3);
            }
        }
        const mergeCountArray = Array.from(mergeCountMap.entries());
        localStorage.setItem('mergeCountMap', JSON.stringify(mergeCountArray));

        setMergedValues([]);
    }


    const mergeTiles = useCallback((tilesRow: (ITile | null)[], help :boolean): (ITile | null)[] => {
        let scoreToAdd = 0;
        // let mergedValues: number[] = [];

        tilesRow.forEach(tile => {
            if (tile !== null) tile.justMerged = false;
        });

        let newRow = [...tilesRow].filter(tile => tile !== null);






        for (let i = 0; i < newRow.length - 1; i++) {
            if (newRow[i] !== null && newRow[i]?.value === newRow[i + 1]?.value) {
                // @ts-ignore
                const mergedValue = newRow[i]?.value * 2;
                scoreToAdd += mergedValue;
                newRow[i] = {id: 0, isNew: false, ...newRow[i], value: mergedValue, hasMerged: true, justMerged: true };
                newRow[i + 1] = null;

                mergedValues.push(mergedValue/2);
            }
        }

        if (!help) {
            simulateScoreAddition(scoreToAdd);
        }

        return newRow.filter(Boolean).concat(new Array(4 - newRow.filter(Boolean).length).fill(null));
    }, [currentScore, bestScore, simulateScoreAddition]);

    const moveLeft = (tiles: (ITile | null)[], help: boolean): (ITile | null)[] => {

        let newTiles = Array(16).fill(null);
        for (let row = 0; row < 4; row++) {
            let rowTiles: (ITile | null)[] = [];
            for (let col = 0; col < 4; col++) {
                const idx = row * 4 + col;
                if (tiles[idx]) {
                    rowTiles.push(tiles[idx]);
                }
            }
            if (help) {
                //console.log("rrrrrr : " + help)
                rowTiles = mergeTiles(rowTiles,true);
            }
            else {
                rowTiles = mergeTiles(rowTiles,false);
            }
            for (let col = 0; col < 4; col++) {
                const idx = row * 4 + col;
                newTiles[idx] = rowTiles[col];
            }
        }

        if (JSON.stringify(tiles) === JSON.stringify(newTiles)) {
            return tiles;
        }


        return newTiles;
    }
    const moveRight = (tiles: (ITile | null)[], help: boolean): (ITile | null)[] => {
        let newTiles = Array(16).fill(null);
        for (let row = 0; row < 4; row++) {
            let rowTiles: (ITile | null)[] = [];
            for (let col = 3; col >= 0; col--) {
                const idx = row * 4 + col;
                if (tiles[idx]) {
                    rowTiles.push(tiles[idx]);
                }
            }
            if (help) {
                rowTiles = mergeTiles(rowTiles,true);
            }
            else {
                rowTiles = mergeTiles(rowTiles,false);
            }
            for (let col = 0; col < 4; col++) {
                const idx = row * 4 + (3 - col);
                newTiles[idx] = rowTiles[col];
            }
        }
        if (JSON.stringify(tiles) === JSON.stringify(newTiles)) {
            return tiles;
        }
        return newTiles;
    }
    const moveUp = (tiles: (ITile | null)[], help: boolean): (ITile | null)[] => {
        let newTiles = Array(16).fill(null);
        for (let col = 0; col < 4; col++) {
            let columnTiles: (ITile | null)[] = [];
            for (let row = 0; row < 4; row++) {
                const idx = row * 4 + col;
                if (tiles[idx]) {
                    columnTiles.push(tiles[idx]);
                }
            }
            if (help) {
                columnTiles = mergeTiles(columnTiles,true);
            }
            else {
                columnTiles = mergeTiles(columnTiles,false);
            }
            for (let row = 0; row < 4; row++) {
                const idx = row * 4 + col;
                newTiles[idx] = columnTiles[row];
            }
        }
        //console.log('moveUp: ')
        //console.log(newTiles)
        if (JSON.stringify(tiles) === JSON.stringify(newTiles)) {
            return tiles;
        }
        return newTiles;
    }
    const moveDown = (tiles: (ITile | null)[], help: boolean): (ITile | null)[] => {
        let newTiles = Array(16).fill(null);
        for (let col = 0; col < 4; col++) {
            let columnTiles: (ITile | null)[] = [];
            for (let row = 3; row >= 0; row--) {
                const idx = row * 4 + col;
                if (tiles[idx]) {
                    columnTiles.push(tiles[idx]);
                }
            }
            if (help) {
                columnTiles = mergeTiles(columnTiles,true);
            }
            else {
                columnTiles = mergeTiles(columnTiles,false);
            }
            for (let row = 0; row < 4; row++) {
                const idx = (3 - row) * 4 + col;
                newTiles[idx] = columnTiles[row];
            }
        }
        if (JSON.stringify(tiles) === JSON.stringify(newTiles)) {
            return tiles;
        }
        return newTiles;
    }
    const moveTiles = useCallback((direction: string, help: boolean) => {
        let movedTiles = [...tiles];

        switch (direction) {
            case 'up':
                movedTiles = moveUp(movedTiles, help);
                break;
            case 'down':
                movedTiles = moveDown(movedTiles, help)
                break;
            case 'left':
                movedTiles = moveLeft(movedTiles, help);
                break;
            case 'right':
                movedTiles = moveRight(movedTiles, help);
                break;
            default:
                break;
        }

        if (JSON.stringify(tiles) !== JSON.stringify(movedTiles)) {
            movedTiles = addRandomTileToTiles(movedTiles);
        }

        if (!help && mergedValues.length > 0) {
            updateMergeCountAfterMove();
        }

        if (help) {
            return movedTiles;
        }

        setTiles(movedTiles);
    }, [tiles, addRandomTileToTiles, moveUp, moveDown, moveLeft, moveRight, updateMergeCountAfterMove]);


    function hasBoardChanged(oldBoard: (ITile | null)[], newBoard: (ITile | null)[]): boolean {
        return !oldBoard.every((tile, index) => tile === newBoard[index]);
    }

    function calculateMergeCount(board: (ITile | null)[]): number {
        let mergeCount = 0;
        board.forEach(tile => {
            if (tile !== null && tile.hasMerged) {
                mergeCount++;
            }
        });
        return mergeCount;
    }
    function countEmptyTiles(board: (ITile | null)[]): number {
        let emptyTileCount = 0;
        if (Array.isArray(board)) {
            board.forEach(row => {
                if (row === null) {
                    emptyTileCount++;
                }
            });
        }
        return emptyTileCount;
    }
    function findBestMove() {
        const directions = ['up', 'down', 'left', 'right'];
        let bestMove = null;
        let maxScore = -1;
        let hasChange = false;
        let gameEnds = false;

        directions.forEach(direction => {
            const oldBoard: (ITile | null)[] = tiles;
            // @ts-ignore
            const newBoard: (ITile | null)[]  = moveTiles(direction, true);
            console.log(newBoard);
            const gameOver: boolean = checkGameOver(newBoard);
            const emptyTiles: number = countEmptyTiles(newBoard);
            let changed: boolean = hasBoardChanged(oldBoard, newBoard);
            let mergeCount: number = calculateMergeCount(newBoard);
            const score: number = emptyTiles + 2 * mergeCount;
            if (!gameOver) {
                if (!isGameOver && score > maxScore && changed) {
                    maxScore = score;
                    bestMove = direction;
                    hasChange = true;
                }
            }
        });

        if (currentScore < 88) {
            console.log("You don't have enough score!");
            return null;
        } else if (gameEnds) {
            console.log("Game ends with every move!");
            return null;
        } else if (!hasChange) {
            console.log("No beneficial move available or game ends with every move!");
            return null;
        } else if (hasUsedHelp) {
            simulateScoreAddition(-88);
            setHasUsedHelp(false);
        }
        return bestMove;
    }





    return (
        // @ts-ignore
        <TilesContext.Provider value={{ tiles, setTiles, moveTiles, setTilesForTest, mergeCounts, resetGame , isNewGame, setIsNewGame, undo, findBestMove, isGameOver, setIsGameOver, isUndo, setIsUndo, setHasUsedHelp, hasUsedHelp,setIsClicked,mergeCountMap,history: tilesHistory}}>
            {children}
        </TilesContext.Provider>
    );

};

export const useTiles = (): TilesContextType => {
    const context = useContext(TilesContext);
    if (context === undefined) {
        throw new Error('useTiles must be used within a TilesProvider');
    }
    return context;
};
