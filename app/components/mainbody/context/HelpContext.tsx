'use client'
import React, { createContext, useContext, useState } from 'react';

interface HelpContextType {
    helpVisible: boolean;
    direction: string;
    helpClicked: boolean;
    showHelp: (direction: string) => void;
    hideHelp: () => void;
    setHelpClicked: (clicked: boolean) => void;
}

const defaultState: HelpContextType = {
    helpVisible: false,
    direction: '',
    helpClicked: false,
    showHelp: () => {},
    hideHelp: () => {},
    setHelpClicked: () => {}
};

const HelpContext = createContext<HelpContextType>(defaultState);

export const useHelp = () => useContext(HelpContext);

interface HelpProviderProps {
    children: React.ReactNode;
}

export const HelpProvider: React.FC<HelpProviderProps> = ({ children }) => {
    const [helpVisible, setHelpVisible] = useState<boolean>(false);
    const [helpClicked, setHelpClicked] = useState<boolean>(false);
    const [direction, setDirection] = useState<string>('');

    const showHelp = (dir: string) => {
        setDirection(dir);
        setHelpVisible(true);
        setTimeout(() => {
            setHelpVisible(false);
        }, 1000);
    };

    const hideHelp = () => {
        setHelpVisible(false);
    };

    return (
        <HelpContext.Provider value={{
            helpVisible,
            direction,
            showHelp,
            hideHelp,
            helpClicked,
            setHelpClicked
        }}>
            {children}
        </HelpContext.Provider>
    );
};
