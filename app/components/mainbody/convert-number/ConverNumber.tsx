type NumberMap = { [key: string]: string };
type Language = 'iran' | 'saudiArabia' | 'china' | 'southKorea';


const persianNumbers: NumberMap = {
    '0': '۰', '1': '۱', '2': '۲', '3': '۳', '4': '۴',
    '5': '۵', '6': '۶', '7': '۷', '8': '۸', '9': '۹',
};

const chineseNumbers: NumberMap = {
    '0': '零', '1': '一', '2': '二', '3': '三', '4': '四',
    '5': '五', '6': '六', '7': '七', '8': '八', '9': '九',
};

const koreanNumbers: NumberMap = {
    '0': '영', '1': '일', '2': '이', '3': '삼', '4': '사',
    '5': '오', '6': '육', '7': '칠', '8': '팔', '9': '구',
};

export const convertNumberByLanguage = (number: string, language: string): string => {
    switch (language) {
        case 'iran':
        case 'saudiArabia':
            return number.split('').map(digit => persianNumbers[digit] || digit).join('');
        case 'china':
            return number.split('').map(digit => chineseNumbers[digit] || digit).join('');
        case 'southKorea':
            return number.split('').map(digit => koreanNumbers[digit] || digit).join('');
        default:
            return number;
    }
};