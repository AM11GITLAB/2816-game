'use client'
import React, {useEffect, useState} from 'react';
import { useTiles } from '@/app/components/mainbody/context/TileContext';
import Tile from "./Tile";
import ScoreBoard from "../scoreboard/ScoreBoard";
import Buttons from "../Buttons";
import GameOverModal from "@/app/components/mainbody/Grid/GameOverModal";
import {useScore} from "@/app/components/mainbody/hooks/useScore";

const GameGrid = () => {
    const { tiles, moveTiles , resetGame, isNewGame, setIsNewGame, isGameOver, setIsGameOver,setHasUsedHelp,setIsClicked} = useTiles();
    const {resetCurrentScore, resetMergedScores,setMergedScores} = useScore();


    const handleKeyDown = (e: KeyboardEvent) => {
        switch (e.key) {
            case 'ArrowUp':
                setIsClicked(true);
                resetMergedScores();
                moveTiles('up',false);
                setHasUsedHelp(true);
                break;
            case 'ArrowDown':
                setIsClicked(true);
                resetMergedScores();
                moveTiles('down', false);
                setHasUsedHelp(true);
                break;
            case 'ArrowLeft':
                setIsClicked(true);
                resetMergedScores();
                moveTiles('left', false);
                setHasUsedHelp(true);
                break;
            case 'ArrowRight':
                setIsClicked(true);
                resetMergedScores();
                moveTiles('right', false);
                setHasUsedHelp(true);
                break;
            default:
                return;
        }
    };

    useEffect(() => {
        window.addEventListener('keydown', handleKeyDown);

        return () => {
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, [handleKeyDown]);



    return (
        <div className="game-grid grid gap-4 ml-10 mr-10">
            <ScoreBoard />
            <Tile tiles={tiles} />
            <Buttons />
            <GameOverModal
                isOpen={isGameOver}
                onClose={() => setIsGameOver(false)}
                onRestart={() => {
                    resetGame();
                    resetCurrentScore();
                    setIsGameOver(false);
                    setIsNewGame(true);
                }}
            />
        </div>
    );
};

export default GameGrid;
