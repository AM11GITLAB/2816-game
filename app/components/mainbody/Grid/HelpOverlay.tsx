import React from 'react';
import './HelpOverlay.css';

interface HelpOverlayProps {
    direction: string;
}


const HelpOverlay: React.FC<HelpOverlayProps> = ({ direction }) => {
    return (
        <div className="overlay">
            <div className={`arrow ${direction}`}></div>
        </div>
    );
};

export default HelpOverlay;

