import React from 'react';
import { Transition } from 'react-transition-group';
import { useScore } from "@/app/components/mainbody/hooks/useScore";
import { useTranslation } from "@/public/translation/translation";

interface GameOverModalProps {
  isOpen: boolean;
  onClose: () => void;
  onRestart: () => void;
}

const GameOverModal: React.FC<GameOverModalProps> = ({ isOpen, onClose, onRestart }) => {
  const transitionStyles = {
    entering: 'visible opacity-100',
    entered: 'visible opacity-100',
    exiting: 'invisible opacity-0',
    exited: 'invisible opacity-0',
    unmounted: 'invisible opacity-0'
  };

  const { t } = useTranslation();
  const { currentScore, bestScore } = useScore();
  let overGameParagraph = '';

  if (currentScore >= bestScore) {
    overGameParagraph = t('ogp1');
  } else if (currentScore < 2000) {
    overGameParagraph = t('ogp2');
  } else if (currentScore >= 2000 && currentScore < 8000) {
    overGameParagraph = t('ogp3');
  } else if (currentScore >= 8000) {
    overGameParagraph = t('ogp4');
  }

  return (
      <Transition in={isOpen} timeout={300}>
        {state => (
            <>
              <div
                  className={`fixed inset-0 z-40 bg-black opacity-50 transition-opacity duration-300 ${isOpen ? 'visible opacity-100' : 'invisible opacity-0'}`}
                  onClick={onClose}
              ></div>

              <div
                  className={`fixed inset-0 z-50 flex items-center justify-center transition-opacity duration-300 ${transitionStyles[state]}`}
                  data-testid="modal-background"
                  onClick={onClose}
              >
                <div className="p-5 bg-white rounded-lg shadow-xl">
                  <h2 className="text-2xl font-bold mb-4">{t('gameOver')}</h2>
                  <p>{overGameParagraph}</p>
                  <div className="mt-4 flex justify-end">
                    <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        onClick={onRestart}
                    >
                      {t('restart')}
                    </button>
                  </div>
                </div>
              </div>
            </>
        )}
      </Transition>
  );
};

export default GameOverModal;
