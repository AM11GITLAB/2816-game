export interface ITile {
    value: number;
    id: number;
    isNew: boolean;
    hasMerged: boolean;
    justMerged?: boolean;
}

export const createTile = (value: number, isNew: boolean): ITile => ({
    value,
    id: Date.now(),
    isNew,
    hasMerged: false,
    justMerged: false
});
