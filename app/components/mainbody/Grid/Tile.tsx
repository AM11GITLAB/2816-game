'use client'
import React from 'react';
import { ITile } from './ITile';
import { convertNumberByLanguage } from "@/app/components/mainbody/convert-number/ConverNumber";
import { useLanguage } from "@/app/components/mainbody/context/LanguagesContext";
import HelpOverlay from "@/app/components/mainbody/Grid/HelpOverlay";
import { useHelp } from "@/app/components/mainbody/context/HelpContext";

export interface Tile16Props {
    tiles: (ITile | null)[];
}

const getBackgroundColorClass = (value: number) => {
    switch (value) {
        case 11:
            return 'bg-indigo-200';
        case 22:
            return 'bg-indigo-300';
        case 44:
            return 'bg-indigo-400';
        case 88:
            return 'bg-indigo-500';
        case 176:
            return 'bg-indigo-600 text-white';
        case 352:
            return 'bg-indigo-700 text-white';
        case 704:
            return 'bg-indigo-800 text-white';
        case 1408:
            return 'bg-indigo-900 text-white';
        case 2816:
            return 'bg-indigo-950 text-white';
        default:
            return 'bg-gray-300';
    }
};

const Tile: React.FC<Tile16Props> = ({ tiles }) => {
    const { language } = useLanguage();
    const { helpVisible, direction } = useHelp();
    let hasDirection = false;
    if (direction) {
        hasDirection = true;
    }

    const textSizeClass = language === 'southKorea' || 'china' ? 'text-3xl' : 'text-4xl';

    return (
        <div className='grid grid-cols-4 gap-3 max-w-lg w-full bg-gray-400 dark:bg-gray-500 m-auto border-2 border-gray-500 dark:border-gray-400 p-3 rounded-md relative'>
            {tiles.map((tile, index) => (
                tile ? (
                    <div key={index}
                         className={`${!tile.justMerged && !tile.isNew && tile.value ? (direction) : ''}
                         ${getBackgroundColorClass(tile.value)} h-28 tile value-${convertNumberByLanguage(tile.value.toString(), language)} ${textSizeClass} flex justify-center items-center rounded-md ${tile.justMerged ? 'animate-pop' : ''}${tile.isNew && !tile.justMerged ? 'animate-fade-in' : ''}`}>
                        {convertNumberByLanguage(tile.value.toString(), language)}
                    </div>
                ) : (
                    <div key={index} className="bg-gray-300 dark:bg-gray-400 h-28 tile empty rounded-md"></div>
                )
            ))}
            {helpVisible && <HelpOverlay direction={direction} />}
        </div>
    );
};

export default Tile;
