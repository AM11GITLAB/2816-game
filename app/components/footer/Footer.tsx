const Footer = () => {
    return <footer>
        <div
            className="w-full bg-neutral-200 p-4 text-center text-neutral-700 dark:bg-neutral-700 dark:text-neutral-200 my-custom-font">
            © 2024 Copyright : 2816.ir
        </div>
    </footer>
};
export default Footer;