import dynamic from 'next/dynamic';
import './Header.css';

const DarkMode = dynamic(() => import('@/app/components/header/DarkMode'), { ssr: false });

const Header = () => {
    return (
        <header>
            <div
                className="flex justify-between bg-neutral-200 p-4 text-neutral-700 dark:bg-neutral-700 dark:text-neutral-200 h-min">
                <div className="flex-1">
                    <h1 className='p-3 text-4xl my-custom-font'>2816.ir</h1>
                </div>
                <div className="flex justify-start">
                    <DarkMode/>
                </div>
            </div>
        </header>
    );
};

export default Header;