'use client'
import React from 'react';
import { useState, useEffect } from 'react';
import {useTranslation} from "@/public/translation/translation";
const DarkMode = () => {
    const {t} = useTranslation();
    const [darkMode, setDarkMode] = useState(() => JSON.parse(localStorage.getItem('darkMode') || 'false'));

    useEffect(() => {
        document.body.classList.toggle('dark', darkMode);
        localStorage.setItem('darkMode', JSON.stringify(darkMode));
    }, [darkMode]);

    const toggleTheme = () => {
        setDarkMode(!darkMode);
    };


    return (
        <button onClick={toggleTheme}
                className="flex items-center flex-row-reverse p-3 rounded-lg focus:outline-none focus:ring transition space-x-2
                text-2xl h-min">
    <span>
        {darkMode ? t('lightMode') : t('darkMode')}
    </span>
            {darkMode ? (
                <img src='/images/icons8-dark-mode-50.png' className='h-10 w-10 filter invert brightness-0 contrast-200'
                     alt='dark-mode'/>
            ) : (
                <img src='/images/icons8-light-mode-78.png' className='h-10 w-10' alt='light-mode'/>
            )}
        </button>

    );
};

export default DarkMode;