import type { Metadata } from "next";
import "./globals.css";
import localFont from "next/font/local";

export const metadata: Metadata = {
  title: "2048 Game",
  description: "a popular game ...",
};

    const winterStormFont = localFont({
  src: "../public/font/WinterStorm.ttf",
});

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
      <html lang="en">
      <body className={`${winterStormFont.className}`}>
      {children}</body>
      </html>
  );
}
