import { Config } from "tailwindcss";
import plugin from 'tailwindcss/plugin';


const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: 'class',
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
            "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        dark: {
          100: "#1a1a1a",
          200: "#333",
        },
      },
    },
    keyframes: {
      shake: {
        '0%, 100%': { transform: 'translateX(0)' },
        '10%, 30%, 50%, 70%, 90%': { transform: 'translateX(-4px)' },
        '20%, 40%, 60%, 80%': { transform: 'translateX(4px)' },
      },
      merge: {
        '0%, 100%': { transform: 'scale(1)' },
        '50%': { transform: 'scale(1.1)' },
      },
      pop: {
        '0%, 100%': { transform: 'scale(1)' },
        '50%': { transform: 'scale(1.2)' },
      },
      slideRight: {
        'from': { transform: 'translateX(-100%)' },
        'to': { transform: 'translateX(0)' },
      },
      slideLeft: {
        'from': { transform: 'translateX(100%)' },
        'to': { transform: 'translateX(0)' },
      },
      slideUp: {
        'from': { transform: 'translateY(100%)' },
        'to': { transform: 'translateY(0)' },
      },
      slideDown: {
        'from': { transform: 'translateY(-100%)' },
        'to': { transform: 'translateY(0)' },
      },
      fadeIn: {
        // @ts-ignore
        '0%': { opacity: 0, transform: 'scale(0.95)' },
        // @ts-ignore
        '100%': { opacity: 1, transform: 'scale(1)' },
      },
      floatUp: {
        // @ts-ignore
        'from': { transform: 'translateY(0px)', opacity: 1 },
        // @ts-ignore
        'to': { transform: 'translateY(-50px)', opacity: 0 },
      }
    },
    animation: {
      'animate-merge': 'merge 0.5s ease-in-out',
      'pop': 'pop 0.5s ease-in-out',
      'slide-right': 'slideRight 0.5s ease-out',
      'slide-left': 'slideLeft 0.5s ease-out',
      'slide-up': 'slideUp 0.5s ease-out',
      'slide-down': 'slideDown 0.5s ease-out',
      'fade-in': 'fadeIn 0.5s ease-out',
      'score-pop': 'scorePop 0.5s ease-in-out',
      'float-up': 'floatUp 1s ease-in-out forwards',
      'float-up-long': 'floatUp 5s ease-in-out forwards',
      'shake': 'shake 1s cubic-bezier(.36,.07,.19,.97) both',
    },
  },

  plugins: [
    plugin(function({ addUtilities }) {
      const newUtilities = {
        '.custom-transition': {
          transition: 'left 100ms ease-in, top 100ms ease-in, visibility 5ms linear 100ms',
        },
        '.custom-transform-visibility': {
          transition: 'transform 140ms cubic-bezier(0, .2, 0, 1.5), visibility 5ms linear 200ms',
        },
      };
      addUtilities(newUtilities);
    }),
  ],
};

export default config;

