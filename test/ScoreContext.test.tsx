import React, {useContext} from 'react';
import {render, fireEvent, screen, act} from '@testing-library/react';
import '@testing-library/jest-dom';
import {ScoreContext, ScoreProvider, ScoreContextType} from "@/app/components/mainbody/context/ScoresContext";

describe('ScoreProvider', () => {
    beforeEach(() => {
        window.localStorage.clear();
        window.localStorage.setItem('currentScore', '0');
    });

    it('should increase currentScore correctly when simulateScoreAddition is called', () => {
        let testScore;
        const TestComponent = () => {
            const { currentScore, simulateScoreAddition } = useContext(ScoreContext) as ScoreContextType;
            testScore = currentScore;
            return <button onClick={() => simulateScoreAddition(10)}>Add Score</button>;
        };

        const { getByText } = render(
            <ScoreProvider>
                <TestComponent />
            </ScoreProvider>
        );

        expect(testScore).toBe(0);
        act(() => {
            fireEvent.click(getByText('Add Score'));
        });
        expect(testScore).toBe(10);
        expect(window.localStorage.getItem('currentScore')).toBe('10');
    });
});
