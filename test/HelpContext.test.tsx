import React from 'react';
import { render, screen, act } from '@testing-library/react';
import { HelpProvider, useHelp } from '@/app/components/mainbody/context/HelpContext';

const TestComponent = () => {
    const help = useHelp();
    return (
        <div>
            <button onClick={() => help.showHelp('north')}>Show North</button>
            <button onClick={help.hideHelp}>Hide Help</button>
            <button onClick={() => help.setHelpClicked(true)}>Set Clicked</button>
            <div data-testid="visibility">{help.helpVisible ? 'Visible' : 'Hidden'}</div>
            <div data-testid="direction">{help.direction}</div>
            <div data-testid="clicked">{help.helpClicked ? 'Clicked' : 'Not Clicked'}</div>
        </div>
    );
};

describe('HelpProvider', () => {
    beforeEach(() => {
        render(
            <HelpProvider>
                <TestComponent />
            </HelpProvider>
        );
    });

    test('renders with initial state correctly', async () => {
        await act(async () => {
            expect(screen.getByTestId('visibility').textContent).toBe('Hidden');
            expect(screen.getByTestId('direction').textContent).toBe('');
            expect(screen.getByTestId('clicked').textContent).toBe('Not Clicked');
        });
    });
    test('changes visibility and direction when showHelp is called', () => {
        act(() => {
            screen.getByText('Show North').click();
        });

        expect(screen.getByTestId('visibility').textContent).toBe('Visible');
        expect(screen.getByTestId('direction').textContent).toBe('north');
    });

    test('hides help when hideHelp is called', () => {
        act(() => {
            screen.getByText('Show North').click();
        });

        act(() => {
            screen.getByText('Hide Help').click();
        });

        expect(screen.getByTestId('visibility').textContent).toBe('Hidden');
    });

    test('sets help clicked state when button is clicked', () => {
        act(() => {
            screen.getByText('Set Clicked').click();
        });

        expect(screen.getByTestId('clicked').textContent).toBe('Clicked');
    });
});
