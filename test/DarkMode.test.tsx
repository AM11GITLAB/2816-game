import React from 'react';
import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';
import DarkMode from '../app/components/header/DarkMode';
import { screen } from '@testing-library/dom';
import '@testing-library/jest-dom';

jest.mock("next/font/local", () => function() {
        return {
            style: {
                fontFamily: 'sans-serif'
            }
        }
    }
);
describe('DarkMode Component', () => {
    beforeEach(() => {
        localStorage.clear();
        document.body.classList.remove('dark');

    });

    test('renders with correct initial mode from localStorage', () => {
        localStorage.setItem('darkMode', 'true');
        render(<DarkMode />);
        expect(document.body.classList.contains('dark')).toBe(true);
        expect(screen.getByRole('button').querySelector('span')).toHaveTextContent('Light Mode');
    });

    test('toggles theme on button click', () => {
        const { getByRole } = render(<DarkMode />);
        const button = getByRole('button');
        fireEvent.click(button);
        expect(document.body.classList.contains('dark')).toBe(true);
        expect(localStorage.getItem('darkMode')).toBe('true');
        fireEvent.click(button);
        expect(document.body.classList.contains('dark')).toBe(false);
        expect(localStorage.getItem('darkMode')).toBe('false');
    });

    test('uses the correct icons for each theme', () => {
        render(<DarkMode />);
        const button = screen.getByRole('button');
        fireEvent.click(button);
        expect(screen.getByAltText('dark-mode')).toBeInTheDocument();
        fireEvent.click(button);
        expect(screen.getByAltText('light-mode')).toBeInTheDocument();
    });
});
