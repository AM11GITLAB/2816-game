import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Languages from '../app/components/mainbody/Languages';
import { useLanguage } from '@/app/components/mainbody/context/LanguagesContext';

jest.mock('next/image', () => ({
    __esModule: true,
    default: (props: React.ImgHTMLAttributes<HTMLImageElement>) => <img {...props} />,
}));

jest.mock('../app/components/mainbody/context/LanguagesContext', () => {
    return {
        useLanguage: jest.fn()
    };
});

const localStorageMock = (function() {
    let store: Record<string, string> = {};
    return {
        getItem: function(key: string): string | null {
            return store[key] || null;
        },
        setItem: function(key: string, value: string): void {
            store[key] = value;
        },
        clear: function(): void {
            store = {};
        }
    };
})();


Object.defineProperty(window, 'localStorage', {
    value: localStorageMock,
});

describe('Languages Component', () => {
    beforeEach(() => {
        localStorageMock.clear();
        jest.clearAllMocks();
        (useLanguage as jest.Mock).mockImplementation(() => ({
            switchLanguage: jest.fn(),
        }));
    });

    it('should render all flags and handle click correctly', () => {
        const { getByAltText } = render(<Languages />);
        const usaFlag = getByAltText('USA');
        fireEvent.click(usaFlag);
        expect(localStorage.getItem('language')).toBe('usa');
        expect(usaFlag.parentElement).toHaveClass('border-gray-500 border-3 shadow-xl');
    });

    it('should load the saved language from localStorage', () => {
        localStorage.setItem('language', 'china');
        const { getByAltText } = render(<Languages />);
        const chinaFlag = getByAltText('China');
        expect(chinaFlag.parentElement).toHaveClass('border-gray-500 border-3 shadow-xl');
    });
});
