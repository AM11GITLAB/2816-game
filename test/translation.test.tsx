import React, { ReactNode } from 'react';
import { renderHook } from '@testing-library/react-hooks';
import { LanguageContext } from '@/app/components/mainbody/context/LanguagesContext';
import { useTranslation } from '@/public/translation/translation';

interface WrapperProps {
    children: ReactNode;
}

jest.mock("next/font/local", () => () => ({
    style: {
        fontFamily: 'sans-serif'
    }
}));

describe('useTranslation Hook', () => {
    it('returns correct translations for the selected language', () => {
        const wrapper = ({ children }: WrapperProps) => (
            <LanguageContext.Provider value={{ language: 'iran', switchLanguage: jest.fn() }}>
                {children}
            </LanguageContext.Provider>
        );

        const { result } = renderHook(() => useTranslation(), { wrapper });

        expect(result.current.t('undo')).toBe('بازگشت');
        expect(result.current.t('newGame')).toBe('بازی جدید');
        expect(result.current.t('help')).toBe('راهنما');
    });

    it('returns the key if translation does not exist', () => {
        const wrapper = ({ children }: WrapperProps) => (
            <LanguageContext.Provider value={{ language: 'germany', switchLanguage: jest.fn() }}>
                {children}
            </LanguageContext.Provider>
        );

        const { result } = renderHook(() => useTranslation(), { wrapper });

        expect(result.current.t('undo')).toBe('undo');
        expect(result.current.t('newGame')).toBe('newGame');
        expect(result.current.t('help')).toBe('help');
    });
});
