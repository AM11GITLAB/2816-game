import { convertNumberByLanguage } from '@/app/components/mainbody/convert-number/ConverNumber';

describe('convertNumberByLanguage', () => {
    it('converts numbers to Persian correctly', () => {
        const result = convertNumberByLanguage('1234567890', 'iran');
        expect(result).toBe('۱۲۳۴۵۶۷۸۹۰');
    });

    it('converts numbers to Chinese correctly', () => {
        const result = convertNumberByLanguage('1234567890', 'china');
        expect(result).toBe('一二三四五六七八九零');
    });

    it('converts numbers to Korean correctly', () => {
        const result = convertNumberByLanguage('1234567890', 'southKorea');
        expect(result).toBe('일이삼사오육칠팔구영');
    });

    it('returns the same number string for unsupported language', () => {
        const result = convertNumberByLanguage('1234567890', 'english');
        expect(result).toBe('1234567890');
    });

    it('handles empty string input', () => {
        const result = convertNumberByLanguage('', 'iran');
        expect(result).toBe('');
    });

    it('ignores non-digit characters and converts only digits', () => {
        const result = convertNumberByLanguage('1a2b3c4d5e', 'china');
        expect(result).toBe('一a二b三c四d五e');
    });
});
