import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ScoreBoard from '../app/components/mainbody/scoreboard/ScoreBoard';

jest.mock("@/public/translation/translation", () => ({
    useTranslation: () => ({ t: (key: string) => key })
}));

jest.mock("@/app/components/mainbody/context/LanguagesContext", () => ({
    useLanguage: () => ({ language: 'en' })
}));

jest.mock("@/app/components/mainbody/hooks/useScore");

jest.mock("@/app/components/mainbody/convert-number/ConverNumber", () => ({
    convertNumberByLanguage: (number: number, language: string) => `${number}-${language}`
}));

beforeEach(() => {
    const { useScore } = require("@/app/components/mainbody/hooks/useScore");
    useScore.mockImplementation(() => ({
        currentScore: 100,
        bestScore: 200,
        mergedScores: 50,
        showAddedScore: true,
        limitedCurrentScore: 95,
        setIs2816: jest.fn(),
        is2816: false,
        setCurrentScoresArr: jest.fn(),
        setLimitedCurrentScoresArr: jest.fn()
    }));
});

describe('ScoreBoard Component', () => {
    it('renders current and best scores correctly', () => {
        render(<ScoreBoard />);
        expect(screen.getByText('currentScore')).toBeInTheDocument();
        expect(screen.getByText('100-en - 95-en')).toBeInTheDocument();
        expect(screen.getByText('highScore')).toBeInTheDocument();
        expect(screen.getByText('200-en')).toBeInTheDocument();
    });

    it('displays added score animation when scores are merged', () => {
        render(<ScoreBoard />);
        expect(screen.getByText('+50-en')).toBeInTheDocument();
    });

    it('displays special message when reaching the limit', () => {
        const { useScore } = require("@/app/components/mainbody/hooks/useScore");
        useScore.mockImplementation(() => ({
            currentScore: 2816,
            bestScore: 200,
            mergedScores: 0,
            showAddedScore: true,
            limitedCurrentScore: 0,
            setIs2816: jest.fn(),
            is2816: true,
            setCurrentScoresArr: jest.fn(),
            setLimitedCurrentScoresArr: jest.fn()
        }));

        render(<ScoreBoard />);
        expect(screen.getByText('2816-en')).toBeInTheDocument();
    });
});
