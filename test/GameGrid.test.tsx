import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import GameGrid from '../app/components/mainbody/Grid/GameGrid';
import {useTiles} from "@/app/components/mainbody/context/TileContext";
import {ScoreProvider} from "@/app/components/mainbody/context/ScoresContext";
import {TilesProvider} from "@/app/components/mainbody/context/TileContext";

const mockSetState = jest.fn();
jest.mock('../app/components/mainbody/Grid/HelpOverlay', () => {
    const MockedHelpOverlay = () => <div>Mocked HelpOverlay</div>;
    MockedHelpOverlay.displayName = 'MockedHelpOverlay';
    return MockedHelpOverlay;
});

jest.mock('../app/components/mainbody/scoreboard/ScoreBoard', () => {
    const MockedScoreBoard = () => <div>Mocked ScoreBoard</div>;
    MockedScoreBoard.displayName = 'MockedScoreBoard';
    return MockedScoreBoard;
});
jest.mock("next/font/local", () => function() {
        return {
            style: {
                fontFamily: 'sans-serif'
            }
        }
    }
);
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useState: jest.fn(),
    useEffect: jest.fn((effect) => effect())
}));
jest.mock('../app/components/mainbody/context/TileContext', () => ({
    useTiles: jest.fn().mockReturnValue({
        tiles: [],
        moveTiles: jest.fn(),
        resetGame: jest.fn(),
        isNewGame: true,
        setIsNewGame: jest.fn(),
        isGameOver: false,
        setIsGameOver: jest.fn(),
        setHasUsedHelp: jest.fn(),
        setIsClicked: jest.fn(),
        resetCurrentScore: jest.fn(),
        resetMergedScores: jest.fn(),
        setMergedScores: jest.fn()
    }),
}));

describe('GameGrid Component', () => {
    beforeEach(() => {
        (React.useState as jest.Mock).mockImplementation((init) => [init, jest.fn()]);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('handles arrow key presses correctly', () => {
        render(
            <ScoreProvider>
                <GameGrid />
            </ScoreProvider>
        );

        fireEvent.keyDown(window, { key: 'ArrowUp' });
        expect(useTiles().moveTiles).toHaveBeenCalledWith('up', false);
        expect(useTiles().setHasUsedHelp).toHaveBeenCalledWith(true);

        fireEvent.keyDown(window, { key: 'ArrowDown' });
        expect(useTiles().moveTiles).toHaveBeenCalledWith('down', false);
        expect(useTiles().setHasUsedHelp).toHaveBeenCalledWith(true);

        fireEvent.keyDown(window, { key: 'ArrowLeft' });
        expect(useTiles().moveTiles).toHaveBeenCalledWith('left', false);
        expect(useTiles().setHasUsedHelp).toHaveBeenCalledWith(true);

        fireEvent.keyDown(window, { key: 'ArrowRight' });
        expect(useTiles().moveTiles).toHaveBeenCalledWith('right', false);
        expect(useTiles().setHasUsedHelp).toHaveBeenCalledWith(true);
    });

    it('resets the game when the GameOverModal restart button is clicked', () => {
        render(
            <ScoreProvider>
                <GameGrid />
            </ScoreProvider>
        );

        fireEvent.click(screen.getByText('Restart' || 'راه اندازی مجدد' || '重新开始' || 'redémarrage' || '재시작' || 'Reanudar' || 'إعادة تشغيل'));
        expect(useTiles().resetGame).toHaveBeenCalled();
        expect(useTiles().setIsGameOver).toHaveBeenCalledWith(false);
        expect(useTiles().setIsNewGame).toHaveBeenCalledWith(true);
    });
});
