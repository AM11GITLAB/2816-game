import React from 'react';
import { render, screen } from '@testing-library/react';
import Tile from '../app/components/mainbody/Grid/Tile';
import { ITile } from '@/app/components/mainbody/Grid/ITile';
import '@testing-library/jest-dom';


jest.mock("@/app/components/mainbody/context/LanguagesContext", () => ({
    useLanguage: () => ({ language: 'EN' })
}));

jest.mock("@/app/components/mainbody/context/HelpContext", () => ({
    useHelp: () => ({ helpVisible: false, direction: null, helpClicked: false })
}));

jest.mock("@/app/components/mainbody/context/TileContext", () => ({
    useTiles: () => ({})
}));

jest.mock("next/font/local", () => function() {
        return {
            style: {
                fontFamily: 'sans-serif'
            }
        }
    }
);

jest.mock('../app/components/mainbody/Grid/HelpOverlay', () => {
    const MockedHelpOverlay = () => <div>Mocked HelpOverlay</div>;
    MockedHelpOverlay.displayName = 'MockedHelpOverlay';
    return MockedHelpOverlay;
});

jest.mock('../app/components/mainbody/scoreboard/ScoreBoard', () => {
    const MockedScoreBoard = () => <div>Mocked ScoreBoard</div>;
    MockedScoreBoard.displayName = 'MockedScoreBoard';
    return MockedScoreBoard;
});

describe('Tile Component', () => {
    it('renders tiles correctly', () => {
        const tiles: (ITile | null)[] = [
            { id: 1, value: 11, justMerged: false, isNew: false, hasMerged: false },
            { id: 2, value: 22, justMerged: true, isNew: false, hasMerged: false },
            { id: 3, value: 44, justMerged: false, isNew: true, hasMerged: false },
            null
        ];

        render(<Tile tiles={tiles} backgroundColor="bg-blue-500" textColor="text-white" number={4} size="h-12" fontFamily="font-sans" fontSize="text-lg" />);

        expect(screen.getByText('11')).toBeInTheDocument();
        expect(screen.getByText('22')).toHaveClass('animate-pop');
        expect(screen.getByText('44')).toHaveClass('animate-fade-in');
        expect(screen.queryByText('empty')).not.toBeInTheDocument();
    });
    it('does not display any tiles if tiles array is empty', () => {
        render(<Tile tiles={[]} backgroundColor="bg-blue-500" textColor="text-white" number={4} size="h-12" fontFamily="font-sans" fontSize="text-lg" />);
        expect(screen.queryByText(/value-/)).not.toBeInTheDocument();
    });
});
