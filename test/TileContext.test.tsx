import React from 'react';
import { render, act } from '@testing-library/react';
import {
    TilesProvider,
    useTiles,
    TilesContextType,
    createEmptyBoard
} from '@/app/components/mainbody/context/TileContext';
import {ScoreProvider} from "@/app/components/mainbody/context/ScoresContext";

const setup = (): TilesContextType => {
    const returnVal: Partial<TilesContextType> = {};
    const TestComponent: React.FC = () => {
        const tilesContext = useTiles();
        Object.assign(returnVal, tilesContext);
        return null;
    };

    render(
        <ScoreProvider>
            <TilesProvider>
                <TestComponent />
            </TilesProvider>
        </ScoreProvider>
    );

    return returnVal as TilesContextType;
}

describe('TilesProvider', () => {
    it('should correctly initialize the game with two tiles', () => {
        const { tiles } = setup();
        const nonNullTiles = tiles.filter(tile => tile !== null);
        expect(nonNullTiles.length).toBe(2);
    });

    it('should reset the game state correctly', () => {
        const { resetGame, tiles } = setup();
        act(() => {
            resetGame();
        });
        const nonNullTiles = tiles.filter(tile => tile !== null);
        expect(nonNullTiles.length).toBe(2);
        expect(tiles).toHaveLength(16);
    });

    it('should start with an empty board', () => {
        const initialBoard = createEmptyBoard();
        expect(initialBoard.every(tile => tile === null)).toBe(true);
    });

    it('should handle undo functionality', () => {
        const { moveTiles, undo, tiles } = setup();
        const initialTiles = [...tiles];
        act(() => {
            moveTiles('left', false);
            undo();
        });
        expect(tiles).toEqual(initialTiles);
    });

    it('should calculate game over correctly', () => {
        const { setIsGameOver, isGameOver } = setup();
        act(() => {
            setIsGameOver(true);
        });

        setTimeout(() => {
            expect(isGameOver).toBe(true);
        }, 0);
    });

    //

});
