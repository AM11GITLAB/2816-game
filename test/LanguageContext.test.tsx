import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { LanguageProvider, useLanguage } from '@/app/components/mainbody/context/LanguagesContext';
import '@testing-library/jest-dom';

jest.mock("next/font/local", () => function() {
        return {
            style: {
                fontFamily: 'sans-serif'
            }
        }
    }
);
const TestComponent = () => {
    const { language, switchLanguage } = useLanguage();
    return (
        <div>
            <span>{language}</span>
            <button onClick={() => switchLanguage('france')}>Switch to French</button>
        </div>
    );
};

describe('LanguageProvider', () => {
    it('should switch languages correctly', () => {
        render(
            <LanguageProvider>
                <TestComponent />
            </LanguageProvider>
        );

        expect(screen.getByText('usa')).toBeInTheDocument();

        fireEvent.click(screen.getByText('Switch to French'));

        expect(screen.getByText('france')).toBeInTheDocument();
    });
});

