import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import GameOverModal from '@/app/components/mainbody/Grid/GameOverModal';
import { useScore } from "@/app/components/mainbody/hooks/useScore";
import '@testing-library/jest-dom';

jest.mock("@/app/components/mainbody/hooks/useScore", () => ({
    useScore: jest.fn().mockImplementation(() => ({
        currentScore: 2500,
        bestScore: 5000,
    })),
}));
jest.mock("@/public/translation/translation", () => ({
    useTranslation: () => ({
        t: (key: string) => key,
    }),
}));

describe('GameOverModal Component', () => {
    it('displays the correct text based on the score', () => {
        const { getByText } = render(<GameOverModal isOpen={true} onClose={() => {}} onRestart={() => {}} />);

        expect(getByText('gameOver')).toBeInTheDocument();
        expect(getByText('ogp3')).toBeInTheDocument();
    });

    it('calls onClose when the background is clicked', () => {
        const mockOnClose = jest.fn();
        const { getByTestId } = render(<GameOverModal isOpen={true} onClose={mockOnClose} onRestart={() => {}} />);

        fireEvent.click(getByTestId('modal-background'));
        expect(mockOnClose).toHaveBeenCalled();
    });

    it('calls onRestart when the restart button is clicked', () => {
        const mockOnRestart = jest.fn();
        const { getByText } = render(<GameOverModal isOpen={true} onClose={() => {}} onRestart={mockOnRestart} />);

        fireEvent.click(getByText('restart'));
        expect(mockOnRestart).toHaveBeenCalled();
    });
});
